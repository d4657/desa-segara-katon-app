import { useContext } from 'react';
import { AuthContext } from '../AppState/AuthProvider';

function useAuth() {
	return useContext(AuthContext);
}

export default useAuth;
