import React, { useEffect, useState } from 'react';
import Title from '../../components/static/Title';

import { Outlet } from 'react-router-dom';

function SuratPage(props) {

	return (
		<div className=''>
			<Title text='Pelayanan Surat' />
			<Outlet/>
		</div>
	);
}

export default SuratPage;
