import React from 'react';
import useAuth from '../../hooks/useAuth';
import KelolaSurat from './component/KelolaSurat';
import SuratBaru from './component/SuratBaru';

function SuratPageIndex(props) {
	const { user, setUser } = useAuth();
	return (
		<div>
			{user.level_pengguna < 2 ? <SuratBaru /> : null}
			<KelolaSurat />
		</div>
	);
}

export default SuratPageIndex;
