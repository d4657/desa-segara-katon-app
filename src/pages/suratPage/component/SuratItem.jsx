import {
	DocumentDownloadIcon,
	DocumentTextIcon,
	InboxIcon,
	PencilIcon,
} from '@heroicons/react/outline';
import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { baseURL } from '../../../API/Api';

function SuratItem(props) {
	const { name, description, id } = props;

	return (
		<div className='flex flex-col gap-1 group '>
			<Link
				to={id}
				className='flex flex-row items-center space-x-2 group-hover:text-main-primary-border justify-between group'
			>
				<div className='inline-flex space-x-2'>
					<InboxIcon className='w-5' />
					<span className='font-bold group-hover:underline'>{name}</span>
				</div>
				<DocumentTextIcon className='w-5' />
			</Link>
			<hr />
			<div className='flex flex-row space-x-1'>
				<button
					onClick={() =>
						window.open(baseURL + '/storage/template/' + id, '_blank')
					}
					className='w-20 h-20 border border-gray-400 rounded flex-shrink-0 text-gray-400 hover:bg-main-primary hover:bg-opacity-50 transition-colors '
				>
					<DocumentDownloadIcon className='w-10 m-auto' />
				</button>
				<span className='h-20 max-h-20 overflow-y-auto'>{description}</span>
			</div>
		</div>
	);
}

export default SuratItem;
