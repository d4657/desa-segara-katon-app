import {
	CheckCircleIcon,
	MinusIcon,
	MinusSmIcon,
	PlusIcon,
	ServerIcon,
	TrashIcon,
	XCircleIcon,
} from '@heroicons/react/outline';
import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import SuratApi from '../../../API/SuratApi';
import ErrorMsg from '../../../components/forms/ErrorMsg';
import SmallLoading from '../../../components/static/SmallLoading';

function SuratBaru(props) {
	const [loading, setLoading] = useState(false);
	const [success, setSuccess] = useState(false);
	const [error, setError] = useState(false);

	const [reqErr, setReqErr] = useState({});
	const [selectedFile, setSelectedFile] = useState(null);

	// ui state
	const [shown, setShown] = useState(false);
	function toggle() {
		setShown(!shown);
	}

	const {
		register,
		handleSubmit,
		watch,
		reset,
		formState: { errors },
	} = useForm({
		criteriaMode: 'all',
	});

	function handleFileChange(e) {
		setSelectedFile(e.target.files[0]);
	}

	function submitSurat(data, e) {
		setLoading(true);
		setReqErr({});

		const form = new FormData();
		form.append('jenis_surat', data.jenis_surat);
		form.append('deskripsi', data.deskripsi);
		form.append('template', selectedFile, selectedFile.name);
		form.append('link_dokumen', data.link_dokumen);

		e.preventDefault();

		SuratApi.createSurat(form)
			.then(async (res) => {
				setLoading(false);
				setSuccess(true);
				reset({
					jenis_surat: '',
					deskripsi: '',
					template: '',
					link_dokumen: '',
				});
				await new Promise((resolve) => setTimeout(resolve, 2000));
				setSuccess(false);
			})

			.catch(async (err) => {
				setReqErr(err.response.data.errors);
				setError(true);
				setLoading(false);
				await new Promise((resolve) => setTimeout(resolve, 2000));
				setError(false);
			});
	}

	return (
		<div className='m-2 flex flex-col border space-y-2 p-2 bg-white'>
			<div className='py-2 inline-flex items-center space-x-2 select-none'>
				<button onClick={toggle} className='btn-primary' id='new-surat'>
					{shown ? <MinusIcon className='w-5' /> : <PlusIcon className='w-5' />}
				</button>
				<label
					htmlFor='new-surat'
					className='font-bold text-lg cursor-pointer hover:text-main-primary-border'
				>
					Tambah Format Surat Baru
				</label>
			</div>

			{shown ? (
				<form onSubmit={handleSubmit(submitSurat)} className='space-y-1'>
					<hr />
					<div className='flex flex-col space-y-1'>
						<div className='flex flex-col lg:flex-row '>
							<label className='pt-1 w-full lg:w-32 ' htmlFor='jenis'>
								Jenis Surat
							</label>
							<input
								{...register('jenis_surat', {
									required: 'Kategori surat harus terisi',
								})}
								type='text'
								id='jenis'
								className='input-primary flex-grow'
							/>
						</div>
						<div className='text-right flex flex-col '>
							<ErrorMsg>
								{errors.jenis_surat ? errors.jenis_surat.message : null}
							</ErrorMsg>
							<ErrorMsg>
								{reqErr.jenis_surat
									? reqErr.jenis_surat.map((msg) => msg)
									: null}
							</ErrorMsg>
						</div>

						<div className='flex flex-col lg:flex-row  '>
							<label className='pt-1 w-32' htmlFor='deskripsi'>
								Deskripsi
							</label>
							<textarea
								{...register('deskripsi', {
									required: 'Deskripsi surat harus terisi',
								})}
								type='text'
								id='deskripsi'
								className='input-primary flex-grow'
							/>
						</div>
						<div className='text-right flex flex-col'>
							<ErrorMsg>
								{errors.deskripsi ? errors.deskripsi.message : null}
							</ErrorMsg>
							<ErrorMsg>
								{reqErr.deskripsi ? reqErr.deskripsi.map((msg) => msg) : null}
							</ErrorMsg>
						</div>

						<div className='flex flex-col lg:flex-row '>
							<label className='pt-1 w-32' htmlFor='template'>
								Template
							</label>
							<input
								{...register('template', {
									required: 'Template arus berupa dokumen',
								})}
								onChange={handleFileChange}
								type='file'
								accept='.docx,.doc,.pdf'
								id='template'
								className='flex-grow input-file'
							/>
						</div>

						<div className='text-right flex flex-col'>
							<ErrorMsg>
								{errors.template ? errors.template.message : null}
							</ErrorMsg>
							<ErrorMsg>
								{reqErr.template ? reqErr.template.map((msg) => msg) : null}
							</ErrorMsg>
						</div>

						<div className='flex flex-col lg:flex-row '>
							<label className='pt-1 w-32' htmlFor='online-text'>
								URL dokumen
							</label>
							<input
								{...register('link_dokumen', {
									required: 'Url dokumen harus terisi',
								})}
								type='text'
								id='online-text'
								className='input-primary flex-grow'
							/>
						</div>
						<div className='text-right flex flex-col'>
							<ErrorMsg>
								{errors.link_dokumen ? errors.link_dokumen.message : null}
							</ErrorMsg>
							<ErrorMsg>
								{reqErr.link_dokumen
									? reqErr.link_dokumen.map((msg) => msg)
									: null}
							</ErrorMsg>
						</div>
					</div>
					<hr />
					<div className='flex flex-row justify-center gap-1 '>
						<button
							type='reset'
							className='py-2 w-40 h-10 btn-secondary inline-flex items-center justify-center space-x-2 '
						>
							<MinusSmIcon className='w-5' />
							<span>Batal</span>
						</button>
						<button
							disabled={loading}
							type='submit'
							className='py-2 w-40 btn-primary h-10 inline-flex items-center justify-center space-x-2'
						>
							{loading ? (
								<SmallLoading />
							) : success ? (
								<CheckCircleIcon className='w-5 m-auto' />
							) : error ? (
								<XCircleIcon className='w-5 m-auto ' />
							) : (
								<>
									<ServerIcon className='w-5' />
									<span>Simpan</span>
								</>
							)}
						</button>
					</div>
				</form>
			) : null}
		</div>
	);
}

export default SuratBaru;
