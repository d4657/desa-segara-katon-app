import {
	ChevronLeftIcon,
	ChevronRightIcon,
	RefreshIcon,
	SearchIcon,
} from '@heroicons/react/outline';
import debounce from 'lodash.debounce';
import React, { useEffect, useRef, useState } from 'react';
import { Link } from 'react-router-dom';
import SuratApi from '../../../API/SuratApi';
import Loading from '../../../components/static/Loading';
import SuratItem from './SuratItem';

function KelolaSurat(props) {
	const [surat, setsurat] = useState(null);
	const [loading, setLoading] = useState(true);
	const [reload, setReload] = useState();

	const [valueSearch, setValueSearch] = useState('');
	const [page, setPage] = useState({});
	const [query, setQuery] = useState({
		perpage: 18,
		page: 1,
		cari: '',
	});

	useEffect(() => {
		setLoading(true);
		SuratApi.getSurat(query)
			.then((res) => {
				setsurat(res.data.payload.data);
				setLoading(false);
				setPage({
					current_page: res.data.payload.current_page,
					last_page: res.data.payload.last_page,
				});
			})
			.catch((err) => {});
	}, [reload, query]);

	function nextPage() {
		if (page.current_page < page.last_page) {
			setQuery({ ...query, page: query.page + 1 });
		}
	}

	function prevPage() {
		if (page.current_page > 1) {
			setQuery({ ...query, page: query.page - 1 });
		}
	}

	const debouncedSave = useRef(
		debounce((nextValue) => setQuery({ ...query, cari: nextValue }), 500)
	).current;

	function handleChangeSearchChange(event) {
		const { value: nextValue } = event.target;
		setValueSearch(nextValue);
		debouncedSave(nextValue);
	}

	return (
		<div className='m-2 flex flex-col border space-y-2 p-2 bg-white'>
			<div className='py-2 items-center flex flex-row justify-between'>
				<span className='font-bold text-lg'>Kelola Surat</span>
				<button onClick={() => setReload(!reload)} className='btn-secondary'>
					<RefreshIcon className='w-5' />
				</button>
			</div>
			<hr />

			<div className='flex flex-col gap-1 lg:flex-row lg:justify-between'>
				<div className='inline-flex'>
					<div className='inline-flex items-center border border-gray-400 border-r-0 w-10'>
						<SearchIcon className='w-5 m-auto' />
					</div>
					<input
						type='text'
						className='input-primary lg:w-72 w-full'
						placeholder='Cari Surat...'
						value={valueSearch}
						onChange={handleChangeSearchChange}
					/>
				</div>
				<div className='space-x-1 inline-flex items-center justify-end '>
					<button
						onClick={prevPage}
						disabled={page.current_page <= 1}
						className='btn-secondary px-1 md:px-4'
					>
						<ChevronLeftIcon className='w-5' />
					</button>
					<span>
						{page.current_page}/{page.last_page}
					</span>
					<button
						onClick={nextPage}
						disabled={page.current_page >= page.last_page}
						className='btn-secondary px-1 md:px-4'
					>
						<ChevronRightIcon className='w-5' />
					</button>
				</div>
			</div>

			<hr />
			{loading ? (
				<Loading />
			) : (
				<div className='grid lg:grid-cols-3 md:grid-cols-2 grid-cols-1 gap-2'>
					{surat.map((value, index) => (
						<div
							className='border p-2 hover:border-main-primary-border rounded hover:bg-main-primary hover:bg-opacity-5  '
							key={index}
						>
							<SuratItem
								id={value.id}
								name={value.jenis_surat}
								description={value.deskripsi}
								url={value.link_dokumen}
								template={value.template}
							/>
						</div>
					))}
				</div>
			)}
		</div>
	);
}

export default KelolaSurat;
