import {
	ChevronLeftIcon,
	DownloadIcon,
	ExternalLinkIcon,
	MinusCircleIcon,
	ServerIcon,
	TrashIcon,
} from '@heroicons/react/outline';
import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { baseURL } from '../../../API/Api';
import SuratApi from '../../../API/SuratApi';
import ErrorMsg from '../../../components/forms/ErrorMsg';
import Loading from '../../../components/static/Loading';
import useAuth from '../../../hooks/useAuth';

function ShowSurat(props) {
	const [pageLoading, setPageLoading] = useState(true);
	const [surat, setSurat] = useState();
	const [reqErr, setReqErr] = useState({});
	const [selectedFile, setSelectedFile] = useState();
	const [showDel, setShowDel] = useState();
	const { user } = useAuth();

	function toggleDel() {
		setShowDel(!showDel);
	}

	const { id } = useParams();
	const navigate = useNavigate();

	const {
		register,
		handleSubmit,
		watch,
		setValue,
		reset,
		getValues,
		formState: { errors },
	} = useForm({ mode: 'onBlur' });

	useEffect(() => {
		setPageLoading(true);
		SuratApi.getSuratById(id)
			.then((res) => {
				const suratData = res.data.payload;
				setSurat(suratData);
				setPageLoading(false);
				if (!suratData) {
					navigate('/surat');
				}
			})
			.catch((err) => {});
	}, [id]);

	useEffect(() => {
		reset(surat);
	}, [surat]);

	function handleSimpan(data, e) {
		e.preventDefault();
		console.log(data);
		setReqErr({});

		const form = new FormData();
		form.append('jenis_surat', data.jenis_surat);
		form.append('deskripsi', data.deskripsi);
		if (selectedFile) {
			form.append('template', selectedFile, selectedFile.name);
		}

		form.append('link_dokumen', data.link_dokumen);

		SuratApi.updateSuratById(form, id)
			.then((res) => {
				console.log(res.data.payload);
			})
			.catch((err) => {
				console.log(err.response.data.errors);
			});
	}

	function handleFileChange(e) {
		setSelectedFile(e.target.files[0]);
	}

	function handleDelete() {
		SuratApi.deleteSuratById(id)
			.then((res) => {
				navigate('/surat');
			})
			.catch((err) => {
				setReqErr({ ...reqErr, ...err.response.data.errors });
			});
	}


	return (
		<div className='bg-white h-full border p-2 mt-2 mx-2 space-y-1 '>
			<div className='py-2 flex flex-row space-x-1 items-center'>
				<Link className='btn-primary' to='/surat '>
					<ChevronLeftIcon className='w-5' />
				</Link>
				<span className='font-bold text-lg'>Edit Surat</span>
			</div>

			<hr />
			{pageLoading ? (
				<Loading />
			) : (
				<form
					onReset={(e) => {
						e.preventDefault();
						reset(surat);
					}}
					onSubmit={handleSubmit(handleSimpan)}
					className='flex flex-col space-y-1'
				>
					<div className='flex flex-col space-y-1'>
						<div className='flex flex-col lg:flex-row '>
							<label className='pt-1 w-full lg:w-32 ' htmlFor='jenis'>
								Jenis Surat
							</label>
							<input
								disabled={user.level_pengguna > 1}
								{...register('jenis_surat', {
									required: 'Kategori surat harus terisi',
								})}
								type='text'
								id='jenis'
								className='input-primary flex-grow'
							/>
						</div>
						<div className='text-right flex flex-col '>
							<ErrorMsg>
								{errors.jenis_surat ? errors.jenis_surat.message : null}
							</ErrorMsg>
							<ErrorMsg>
								{reqErr.jenis_surat
									? reqErr.jenis_surat.map((msg) => msg)
									: null}
							</ErrorMsg>
						</div>

						<div className='flex flex-col lg:flex-row  '>
							<label className='pt-1 w-32' htmlFor='deskripsi'>
								Deskripsi
							</label>
							<textarea
								disabled={user.level_pengguna > 1}
								{...register('deskripsi', {
									required: 'Deskripsi surat harus terisi',
								})}
								type='text'
								id='deskripsi'
								className='input-primary flex-grow'
							/>
						</div>
						<div className='text-right flex flex-col'>
							<ErrorMsg>
								{errors.deskripsi ? errors.deskripsi.message : null}
							</ErrorMsg>
							<ErrorMsg>
								{reqErr.deskripsi ? reqErr.deskripsi.map((msg) => msg) : null}
							</ErrorMsg>
						</div>

						<div className='flex flex-col lg:flex-row '>
							<label className='pt-1 w-32' htmlFor=''>
								Template
							</label>
							<div className='flex flex-row space-x-2 flex-grow '>
								<button
									onClick={() =>
										window.open(
											baseURL + '/storage/template/' + surat.id,
											'_blank'
										)
									}
									className='btn-secondary inline-flex items-center space-x-1'
								>
									<DownloadIcon className='w-5' />
									<span>Download</span>
								</button>
								<input
									disabled={user.level_pengguna > 1}
									onChange={handleFileChange}
									type='file'
									accept='.docx,.doc,.pdf'
									id='template'
									className='input-file w-full'
								/>
							</div>
						</div>

						<div className='text-right flex flex-col'>
							<ErrorMsg>
								{errors.template ? errors.template.message : null}
							</ErrorMsg>
							<ErrorMsg>
								{reqErr.template ? reqErr.template.map((msg) => msg) : null}
							</ErrorMsg>
						</div>

						<div className='flex flex-col lg:flex-row '>
							<label className='pt-1 w-32' htmlFor='online-text'>
								URL dokumen
							</label>
							<input
								disabled={user.level_pengguna > 1}
								{...register('link_dokumen', {
									required: 'Url dokumen harus terisi',
								})}
								type='text'
								id='online-text'
								className='input-primary flex-grow'
							/>
						</div>
						<div className='text-right flex flex-col'>
							<ErrorMsg>
								{errors.link_dokumen ? errors.link_dokumen.message : null}
							</ErrorMsg>
							<ErrorMsg>
								{reqErr.link_dokumen
									? reqErr.link_dokumen.map((msg) => msg)
									: null}
							</ErrorMsg>
						</div>
					</div>
					<hr />

					{user.level_pengguna < 2 ? (
						<div className='flex flex-row justify-center gap-1 '>
							<div className='relative'>
								<button
									onClick={toggleDel}
									type='button'
									className='btn-danger h-10 w-32 inline-flex items-center justify-center space-x-2'
								>
									<TrashIcon className='w-5' />
									<span>Hapus</span>
								</button>
								{showDel ? (
									<div className='absolute flex flex-col p-1 gap-1 border bg-white shadow'>
										<span>Yakin ingin menghapus surat?</span>
										<div className='grid grid-cols-2 gap-1'>
											<button onClick={toggleDel} className='btn-secondary'>
												Batal
											</button>
											<button onClick={handleDelete} className='btn-danger'>
												Hapus
											</button>
										</div>
									</div>
								) : null}
							</div>

							<button
								type='reset'
								className='btn-secondary h-10 w-32 inline-flex items-center justify-center space-x-2'
							>
								<MinusCircleIcon className='w-5' />
								<span>Bersihkan</span>
							</button>
							<button
								type='submit'
								className='btn-primary h-10 w-32 inline-flex items-center justify-center space-x-2'
							>
								<ServerIcon className='w-5' />
								<span>Simpan</span>
							</button>
						</div>
					) : null}
	
					<div className='py-2 items-center flex flex-row space-x-1'>
						<span className='font-bold text-lg'>Document viewer</span>
						<button
							type='button'
							onClick={(e) => {
								window.open(surat.link_dokumen, '_blank');
							}}
						>
							<ExternalLinkIcon className='w-5' />
						</button>
					</div>
					<div className='border flex flex-col h-full mb-16'>
						<iframe
							className='web-editor'
							src={surat.link_dokumen}
							title='W3Schools Free Online Web Tutorials'
						></iframe>
					</div>
				</form>
			)}
		</div>
	);
}

export default ShowSurat;
