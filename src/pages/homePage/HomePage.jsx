import React from 'react';
import { Link } from 'react-router-dom';
import LoginImg from '../../assets/images/login.png';
import useAuth from '../../hooks/useAuth';

function HomePage(props) {
	const { user } = useAuth();

	return (
		<div className='flex flex-col md:flex-row h-full bg-white border p-2'>
			<div className='md:w-full lg:w-2/5 md:grid'>
				<img className='w-3/4 md:m-auto' src={LoginImg} alt='login_img'></img>
			</div>

			<div className='w-3/5 grid'>
				<div className='m-auto space-y-1'>
					<h1 className='text-4xl font-bold'>
						Layanan Electronic Form Desa Segara Katon
					</h1>
					<p>
						Layanan pembuatan surat, pengajuan surat, dan tandatangan online
						Desa Segara Katon
					</p>

					{user ? (
						<div className='flex flex-col'>
							<span className='font-bold text-main-primary'>
								Telah masuk sebagai {user.nama_lengkap}
							</span>
							<Link to='/e-sign'>
								<button className='btn-secondary py-2 w-80 mr-1'>
									Kembali ke Layanan
								</button>
							</Link>
						</div>
					) : (
						<div>
							<Link to='/login'>
								<button className='btn-secondary py-2 w-40 mr-1'>Masuk</button>
							</Link>
							<Link to='/register'>
								<button className='btn-secondary py-2 w-40'>Register</button>
							</Link>
						</div>
					)}
				</div>
			</div>
		</div>
	);
}

export default HomePage;
