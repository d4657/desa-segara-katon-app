import {
	EyeIcon,
	EyeOffIcon,
	LockClosedIcon,
	UserIcon,
} from '@heroicons/react/outline';
import React, { useEffect, useRef, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useLocation, useNavigate } from 'react-router-dom';
import { setPrefix } from '../../API/Api';
import UserApi from '../../API/UserApi';
import ErrorMsg from '../../components/forms/ErrorMsg';
import OkModal from '../../components/modals/OkModal';

function RegisterPage(props) {
	const [reqError, setReqError] = useState({});
	const [user, setUser] = useState(null);
	const [showPass, setShowPass] = useState(false);
	const [showModalReg, setShowModalReg] = useState(false);
	const location = useNavigate();

	const {
		register,
		handleSubmit,
		watch,
		formState: { errors },
	} = useForm({
		criteriaMode: 'all',
	});

	function toggleModalReg() {
		setShowModalReg(!showModalReg);
		location('/login', { from: '/' });
	}

	function registerUser(data, e) {
		UserApi.register(data)
			.then((res) => {
				setShowModalReg(!showModalReg);
			})
			.catch((e) => {
				setReqError(e.response.data.errors);
			});
	}

	useEffect(() => {
		UserApi.profile()
			.then((res) => {
				setUser(res.data.payload);
				setPrefix(res.data.payload.level_pengguna);
				location('/profil');
			})

			.catch((err) => {
				if (err.response.status === 401) {
					console.log('aaa');
					localStorage.removeItem('access_token');
				}
			});
	}, []);

	const password = useRef({});
	password.current = watch('password', '');

	return (
		<div className='grid h-full'>
			{showModalReg ? (
				<OkModal
					title='Registrasi pengguna berhasil'
					message='Pengguna baru berhasil diregistrasi, lanjutkan ke proses login untuk menggunakan fitur utama web.'
					callback={toggleModalReg}
				/>
			) : null}
			<div className='w-full md:w-128  m-auto bg-white border p-5 space-y-2'>
				<div className='text-2xl text-center py-3 font-bold'>
					Daftar Sebagai Pengguna
				</div>
				<hr />
				<form
					onSubmit={handleSubmit(registerUser)}
					className='flex flex-col gap-3'
				>
					<div className='space-y-2'>
						<div className='inline-flex items-center'>
							<UserIcon className='w-5' />
							<label className='font-bold'>Identitas Diri</label>
						</div>
						<div className='flex flex-col gap-2 pl-2 border-l'>
							<div className='flex flex-col'>
								{/* nama_lengkap */}
								<label>Nama Lengkap</label>
								<input
									{...register('nama_lengkap', {
										required: 'Nama lengkap harus terisi',
										minLength: {
											value: 3,
											message: 'Nama Lengkap minimal terdiri dari 6 karakter',
										},
									})}
									placeholder='Masukan nama lengkap'
									className='input-primary'
									type={'text'}
								/>
								<ErrorMsg>
									{errors.nama_lengkap ? errors.nama_lengkap.message : null}
								</ErrorMsg>
								<ErrorMsg>
									{reqError.nama_lengkap
										? reqError.nama_lengkap.map((msg) => msg)
										: null}
								</ErrorMsg>
							</div>

							<div className='flex flex-col'>
								<label>Username</label>
								<input
									{...register('username', {
										required: 'Username harus terisi',
										minLength: {
											value: 6,
											message: 'Username minimal terdiri dari 6 karakter',
										},
										maxLength: {
											value: 20,
											message: 'Username maksimial terdiri dari 20 karakter',
										},
										pattern: {
											value:
												/^(?=.{6,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$/,
											message: 'Format username tidak valid',
										},
									})}
									placeholder='Masukan username'
									className='input-primary'
									type={'text'}
								/>
								<ErrorMsg>
									{errors.username ? errors.username.message : null}
								</ErrorMsg>
								<ErrorMsg>
									{reqError.username
										? reqError.username.map((msg) => msg)
										: null}
								</ErrorMsg>
							</div>

							<div className='flex flex-col'>
								<label>Email</label>
								<input
									{...register('email', {
										required: 'Email harus terisi',
										pattern: {
											value:
												/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
											message: 'Format email tidak valid',
										},
									})}
									placeholder='Masukan email'
									className='input-primary'
									type={'text'}
								/>
								<ErrorMsg>
									{errors.email ? errors.email.message : null}
								</ErrorMsg>
								<ErrorMsg>
									{reqError.email ? reqError.email.map((msg) => msg) : null}
								</ErrorMsg>
							</div>
						</div>
					</div>

					<div className='space-y-2'>
						<div className='inline-flex items-center'>
							<LockClosedIcon className='w-5' />
							<label className='font-bold'>Keamanan</label>
						</div>

						<div className='flex flex-col gap-2 pl-2 border-l'>
							<div className='flex flex-col'>
								<label className=''>Password</label>
								<div className='flex flex-row'>
									<input
										{...register('password', {
											required: 'Password harus terisi',
											pattern: {
												value: /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$/,
												message: 'Format password tidak sesuai',
											},
										})}
										placeholder='Masukan password'
										className='input-primary w-full'
										type={showPass ? 'text' : 'password'}
									/>
									<button
										type='button'
										onClick={() => setShowPass(!showPass)}
										className='btn-secondary border-gray-400 rounded-none'
									>
										{showPass ? (
											<EyeIcon className='w-5' />
										) : (
											<EyeOffIcon className='w-5' />
										)}
									</button>
								</div>
								<ErrorMsg>
									{errors.password ? errors.password.message : null}
								</ErrorMsg>
								<ErrorMsg>
									{reqError.password
										? reqError.password.map((msg) => msg)
										: null}
								</ErrorMsg>
							</div>

							<div className='flex flex-col'>
								<label className=''>Konfimasi Password</label>
								<div className='flex flex-row'>
									<input
										{...register('password_confirmation', {
											validate: (value) =>
												value === password.current || 'Password tidak sama',
										})}
										placeholder='Konfirmasi password'
										className='input-primary w-full'
										type={showPass ? 'text' : 'password'}
									/>
									<button
										type='button'
										onClick={() => setShowPass(!showPass)}
										className='btn-secondary border-gray-400 rounded-none'
									>
										{showPass ? (
											<EyeIcon className='w-5' />
										) : (
											<EyeOffIcon className='w-5' />
										)}
									</button>
								</div>

								<ErrorMsg>
									{errors.password_confirmation
										? errors.password_confirmation.message
										: null}
								</ErrorMsg>
								<ErrorMsg>
									{reqError.password_confirmation
										? reqError.password_confirmation.map((msg) => msg)
										: null}
								</ErrorMsg>
							</div>
						</div>
					</div>

					<hr />

					<div className='grid'>
						<button type='submit' className='btn-primary p-2 '>
							Register
						</button>
					</div>
				</form>
			</div>
		</div>
	);
}

export default RegisterPage;
