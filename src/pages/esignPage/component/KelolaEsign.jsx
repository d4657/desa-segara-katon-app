import {
	AdjustmentsIcon,
	ChevronDownIcon,
	ChevronLeftIcon,
	ChevronRightIcon,
	ChevronUpIcon,
	RefreshIcon,
	SearchIcon,
	XCircleIcon,
} from '@heroicons/react/outline';
import debounce from 'lodash.debounce';
import React, { useEffect, useRef, useState } from 'react';
import UsersESignApi from '../../../API/UsersESignApi';
import Loading from '../../../components/static/Loading';
import useAuth from '../../../hooks/useAuth';
import ESignDikirim from './ESignDikirim';
import ESignDiterima from './ESignDiterima';
import ESignPribadi from './ESignPribadi';

function KelolaEsign(props) {
	const [loading, setLoading] = useState(true);
	const [reload, setReload] = useState(false);
	const [esigns, setEsigns] = useState([]);
	const [valueSearch, setValueSearch] = useState('');
	const [showCat, setShowCat] = useState(false);

	function catToggle() {
		setShowCat(!showCat);
	}

	const [query, setQuery] = useState({
		perpage: 10,
		page: 1,
		order: 'desc',
		judul: '',
	});

	const [page, setPage] = useState({ current_page: 1, last_page: 1 });

	function nextPage() {
		if (page.current_page < page.last_page) {
			setQuery({ ...query, page: query.page + 1 });
		}
	}

	function prevPage() {
		if (page.current_page > 1) {
			setQuery({ ...query, page: query.page - 1 });
		}
	}

	useEffect(() => {
		UsersESignApi.getAllUsersEsign(query)
			.then((res) => {
				const payload = res.data.payload;
				setEsigns(payload.data);
				setPage({
					current_page: payload.current_page,
					last_page: payload.last_page,
				});
				setLoading(false);
			})
			.catch((err) => {});
	}, [reload, query]);

	const debouncedSave = useRef(
		debounce((nextValue) => setQuery({ ...query, judul: nextValue }), 1000)
	).current;

	function handleChangeSearchChange(event) {
		const { value: nextValue } = event.target;
		setValueSearch(nextValue);
		debouncedSave(nextValue);
	}

	return (
		<div>
			<div className='m-2 border flex flex-col p-2 space-y-1 bg-white'>
				<div className='py-2 items-center flex flex-row justify-between'>
					<span className='font-bold text-lg'>Kelola E-Signature</span>
					<div></div>
					<button onClick={() => setReload(!reload)} className='btn-secondary'>
						<RefreshIcon className='w-5' />
					</button>
				</div>
				<hr />
				<div className='flex flex-col lg:flex-row justify-between gap-1'>
					<div className='flex flex-row space-x-1'>
						<div className='flex flex-col w-full lg:w-72  gap-1'>
							<span className='font-bold'>Judul / Perihal </span>
							<div className='inline-flex'>
								<div className='inline-flex items-center border border-gray-400 border-r-0 w-10'>
									<SearchIcon className='w-5 m-auto' />
								</div>
								<input
									type='text'
									className='input-primary lg:w-72 w-full'
									placeholder='Cari judul/perihal'
									onChange={handleChangeSearchChange}
									value={valueSearch}
								/>
							</div>
						</div>
					</div>
					<div className='flex flex-row items-end space-x-2 justify-end'>
						<button
							onClick={prevPage}
							disabled={page.current_page <= 1}
							className='btn-secondary px-1 md:px-4'
						>
							<ChevronLeftIcon className='w-5' />
						</button>
						<span>
							{page.current_page}/{page.last_page}
						</span>
						<button
							onClick={nextPage}
							disabled={page.current_page >= page.last_page}
							className='btn-secondary px-1 md:px-4'
						>
							<ChevronRightIcon className='w-5' />
						</button>
					</div>
				</div>
				<hr />
				<div className='overflow-x-auto'>
					<table className='table table-fixed border border-collapse w-full'>
						<thead>
							<tr>
								<th className='border bg-gray-50 py-2 px-1 w-12'>Acc</th>
								<th
									onClick={(e) => {
										if (query.order === 'desc') {
											setQuery({ ...query, order: 'asc' });
										} else {
											setQuery({ ...query, order: 'desc' });
										}
									}}
									className='border bg-gray-50 py-2 px-1 w-64 cursor-pointer '
								>
									<div className='inline-flex items-center space-x-2'>
										<span>Waktu</span>
										{query.order === 'desc' ? (
											<ChevronDownIcon className='w-4' />
										) : (
											<ChevronUpIcon className='w-4' />
										)}
									</div>
								</th>
								<th className='border bg-gray-50 py-2 px-1 lg:w-full w-128'>
									Keterangan
								</th>
							</tr>
						</thead>

						{loading ? (
							<tbody>
								<tr>
									<td colSpan={3} className='p-1'>
										<Loading />
									</td>
								</tr>
							</tbody>
						) : (
							<tbody>
								{esigns.map((data, index) => (
									<ShowEsign data={data} key={index} />
								))}
							</tbody>
						)}
					</table>
				</div>
			</div>
		</div>
	);
}

function ShowEsign(props) {
	const { data } = props;
	const { user } = useAuth();

	if (data.id_pengirim === data.id_penerima) {
		return <ESignPribadi id={data.id} data={data} />;
	}

	if (user.id === data.id_penerima) {
		return <ESignDiterima id={data.id} data={data} />;
	}

	if (user.id === data.id_pengirim) {
		return <ESignDikirim id={data.id} data={data} />;
	}

	return null;
}

export default KelolaEsign;
