import {
	CheckCircleIcon,
	CheckIcon,
	ChevronDoubleDownIcon,
	DocumentIcon,
	InboxInIcon,
	PencilIcon,
	SwitchHorizontalIcon,
} from '@heroicons/react/outline';
import React from 'react';
import { useNavigate } from 'react-router-dom';

function ESignDiterima(props) {
	const { data } = props;

	const options = {
		weekday: 'long',
		year: 'numeric',
		month: 'short',
		day: 'numeric',
		hour: 'numeric',
		minute: 'numeric',
	};

	const navigate = useNavigate();
	function tampilkanEsign() {
		navigate(data.id);
	}

	return (
		<tr
			onClick={tampilkanEsign}
			className='border border-collapse hover:bg-main-primary hover:bg-opacity-5 cursor-pointer'
		>
			<td className='border p-1'>
				<button
					className={
						'w-8 h-8 border rounded-none ' +
						(data.waktu_diterima ? 'btn-primary' : 'btn-secondary')
					}
				>
					{data.waktu_diterima ? <CheckIcon className='w-full' /> : null}
				</button>
			</td>
			<td className='p-1 flex flex-col space-y-1'>
				<span className='font-bold text-main-primary-border'>
					{new Date(data.updated_at).toLocaleString('id-ID', options)}
				</span>
				<button className='inline-flex items-center space-x-2 text-gray-700'>
					<ChevronDoubleDownIcon className='w-5' />
					<span>{data.pengirim_simple.nama_lengkap}</span>
				</button>
				<span className='text-xs text-gray-400 italic'>{data.id}</span>
			</td>
			<td className='border p-1'>
				<span className='font-bold'>{data.judul}</span>
				<br />
				<span>{data.perihal}</span>
				<div className='text-xs space-x-1'>
					{data.waktu_diterima ? (
						<button className='inline-flex items-center p-1 text-main-acc space-x-1 '>
							<CheckCircleIcon className='w-5' />
							<span>Diterima</span>
						</button>
					) : (
						<button className='inline-flex items-center p-1 text-main-warning-border space-x-1 '>
							<InboxInIcon className='w-5' />
							<span>Baru</span>
						</button>
					)}
				</div>
			</td>
		</tr>
	);
}

export default ESignDiterima;
