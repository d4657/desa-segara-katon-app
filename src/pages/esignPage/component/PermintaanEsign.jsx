import {
	CheckCircleIcon,
	CloudUploadIcon,
	MinusIcon,
	PlusIcon,
	XCircleIcon,
} from '@heroicons/react/outline';
import debounce from 'lodash.debounce';
import React, { useEffect, useRef, useState } from 'react';
import { useForm } from 'react-hook-form';
import UserApi from '../../../API/UserApi';
import UsersESignApi from '../../../API/UsersESignApi';
import ErrorMsg from '../../../components/forms/ErrorMsg';
import SmallLoading from '../../../components/static/SmallLoading';

function PermintaanEsign(props) {
	const [shown, setShown] = useState(false);
	const [staffs, setStaffs] = useState([]);
	const [focus, setFocus] = useState(false);
	const [cariNama, setCariNama] = useState({ cari_nama: '' });

	const [success, setSuccess] = useState(false);
	const [loading, setLoading] = useState(false);
	const [error, setError] = useState(false);

	const [reqErr, setErrReq] = useState({});

	function toggle() {
		setShown(!shown);
	}

	const defaultValues = {
		judul: '',
		penerima: '',
		id_penerima: '',
		keperluan_pribadi: false,
		perihal: '',
		waktu_valid: new Date()
			.toLocaleString('en-GB')
			.substring(0, 10)
			.split('/')
			.reverse()
			.join('-'),
	};

	const {
		register,
		handleSubmit,
		setValue,
		resetField,
		reset,
		getValues,

		formState: { errors },
	} = useForm({
		criteriaMode: 'all',
		defaultValues,
	});

	useEffect(() => {
		if (focus && cariNama.cari_nama) {
			UserApi.findStaffByName(cariNama).then((res) => {
				const payload = res.data.payload;
				setStaffs(payload);
			});
		}
	}, [cariNama, focus]);

	function submitPemintaanEsign(data, e) {
		e.preventDefault();
		setLoading(true);
		setSuccess(false);
		setError(false);
		UsersESignApi.createUsersEsign(data)
			.then(async (res) => {
				console.log(res.data.payload);
				reset(defaultValues);
				setCariNama({ cari_nama: '' });
				setSuccess(true);
				setLoading(false);
				await new Promise((resolve) => setTimeout(resolve, 2000));
				setSuccess(false);
			})

			.catch(async (err) => {
				setErrReq(err.payload.data.errors);
				setLoading(false);
				setSuccess(false);
				setError(true);
				await new Promise((resolve) => setTimeout(resolve, 2000));
				setError(false);
			});

		reset(defaultValues);
	}

	const debouncedSave = useRef(
		debounce((nextValue) => setCariNama({  cari_nama: nextValue }), 500)
	).current;

	return (
		<div className='m-2 border flex flex-col p-2 space-y-1 bg-white'>
			<div className='py-2 inline-flex items-center space-x-2 select-none'>
				<button onClick={toggle} className='btn-primary' id='perm-ttd'>
					{shown ? <MinusIcon className='w-5' /> : <PlusIcon className='w-5' />}
				</button>
				<label
					htmlFor={'perm-ttd'}
					className='font-bold text-lg cursor-pointer hover:text-main-primary-border'
				>
					Ajukan Permintaan E-Sign
				</label>
			</div>

			{shown ? (
				<form
					onSubmit={handleSubmit(submitPemintaanEsign, async (e) => {
						setError(true);
						await new Promise((resolve) => setTimeout(resolve, 2000));
						setError(false);
					})}
					className='space-y-1'
				>
					<hr />
					<div className='flex lg:flex-row flex-col '>
						<label className='py-1 w-60' htmlFor=''>
							Judul
						</label>
						<input
							{...register('judul', {
								required: 'Judul E-Sign harus terisi',
							})}
							placeholder='Masukan judul'
							type='text'
							className='input-primary flex-grow'
						/>
					</div>
					<div className='text-right'>
						<ErrorMsg>{errors.judul ? errors.judul.message : null}</ErrorMsg>
						<ErrorMsg>
							{reqErr.judul ? errors.judul.map((msg) => msg) : null}
						</ErrorMsg>
					</div>

					<div className='flex lg:flex-row flex-col '>
						<label className='py-1 w-60' htmlFor=''>
							Ditujukan kepada
						</label>
						<div className='flex flex-col flex-grow space-y-1 '>
							<div className='flex flex-col relative'>
								<input
									{...register('penerima', {
										validate: {
											required: (value) => {
												if (!value && !getValues('keperluan_pribadi')) {
													return 'Masukan penerima jika bukan merupakan keperluan pribadi';
												}
												return true;
											},
										},
									})}
									value={cariNama.cari_nama}
									onChange={(e) => setCariNama({ cari_nama: e.target.value })}
									onFocus={() => setFocus(true)}
									onBlur={async () => {
										await new Promise((resolve) => setTimeout(resolve, 200));
										setFocus(false);
									}}
									autoComplete='off'
									placeholder='Ketikan nama'
									type='text'
									className='input-primary'
								/>
								{staffs && focus ? (
									cariNama.cari_nama ? (
										<div className='absolute shadow w-full top-8 bg-white max-h-60 border border-main-primary overflow-y-auto flex flex-col p-1 '>
											{staffs.map((staff, index) => (
												<button
													type='button'
													key={index}
													onClick={(e) => {
														setValue('penerima', staff.nama_lengkap);
														setValue('id_penerima', staff.id);
														setCariNama({ cari_nama: staff.nama_lengkap });
													}}
													className='text-left hover:bg-main-primary hover:bg-opacity-10'
												>
													{staff.nama_lengkap}
												</button>
											))}
										</div>
									) : (
										<div className='absolute shadow w-full top-8 bg-white max-h-60 border border-main-primary overflow-y-auto flex flex-col p-1 '>
											Ketik setidaknya 1 huruf....
										</div>
									)
								) : null}
							</div>

							<div className='text-right'>
								<ErrorMsg>
									{errors.penerima ? errors.penerima.message : null}
								</ErrorMsg>
								<ErrorMsg>
									{reqErr.id_penerima
										? errors.id_penerima.map((msg) => msg)
										: null}
								</ErrorMsg>
							</div>

							<span>
								<input
									{...register('keperluan_pribadi')}
									type='checkbox'
									id='diri'
									onChange={(e) => {
										if (e.target.value) {
											reset({
												penerima: null,
												id_penerima: null,
											});

											setCariNama({ cari_nama: '' });
										}
									}}
									className='mr-1 checkbox'
								/>

								<label htmlFor='diri'>Keperluan Pribadi</label>
								<div>
									<ErrorMsg>
										{errors.keperluan_pribadi
											? errors.keperluan_pribadi.message
											: null}
									</ErrorMsg>
									<ErrorMsg>
										{reqErr.keperluan_pribadi
											? errors.keperluan_pribadi.map((msg) => msg)
											: null}
									</ErrorMsg>
								</div>
							</span>
						</div>
					</div>

					<div className='flex lg:flex-row flex-col '>
						<label className='py-1 w-60' htmlFor=''>
							Perihal
						</label>
						<textarea
							{...register('perihal', {
								required: 'Perihal permintaan tandatangan harus terisi',
							})}
							placeholder='Masukan perihal'
							className='input-primary flex-grow'
						/>
					</div>
					<div className='text-right'>
						<ErrorMsg>
							{errors.perihal ? errors.perihal.message : null}
						</ErrorMsg>
						<ErrorMsg>
							{reqErr.perihal ? errors.perihal.map((msg) => msg) : null}
						</ErrorMsg>
					</div>

					<div className='flex lg:flex-row flex-col '>
						<label className='py-1 w-60' htmlFor=''>
							Tanggal
						</label>
						<input
							{...register('waktu_valid', {
								required: 'Tanggal valid E-Sign harus terisi',
							})}
							type='date'
							className='input-primary lg:w-72 w-full'
						/>
						<ErrorMsg>
							{errors.waktu_valid ? errors.waktu_valid.message : null}
						</ErrorMsg>
						<ErrorMsg>
							{reqErr.waktu_valid ? errors.waktu_valid.map((msg) => msg) : null}
						</ErrorMsg>
					</div>

					<hr />
					<div className='flex justify-center'>
						<button
							type='submit'
							className='btn-primary p-2 w-36 justify-center border inline-flex items-center space-x-2'
						>
							{loading ? (
								<SmallLoading />
							) : success ? (
								<CheckCircleIcon className='w-5 m-auto' />
							) : error ? (
								<XCircleIcon className='w-5 m-auto ' />
							) : (
								<>
									<CloudUploadIcon className='w-5' />
									<span>Ajukan</span>
								</>
							)}
						</button>
					</div>
				</form>
			) : null}
		</div>
	);
}

export default PermintaanEsign;
