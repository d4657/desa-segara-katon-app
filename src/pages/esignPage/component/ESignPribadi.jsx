import {
	CheckIcon,
	FingerPrintIcon,
	PencilIcon,
	TrashIcon,
} from '@heroicons/react/outline';
import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';

function ESignPribadi(props) {
	const { data } = props;

	const options = {
		weekday: 'long',
		year: 'numeric',
		month: 'short',
		day: 'numeric',
		hour: 'numeric',
		minute: 'numeric',
	};

	const navigate = useNavigate();
	function tampilkanEsign() {
		console.log(data.id)
		navigate(data.id);
	}

	return (
		<tr
			onClick={tampilkanEsign}
			className='border border-collapse hover:bg-main-primary hover:bg-opacity-5 cursor-pointer'
		>
			<td className='border p-1'>
				
			</td>
			<td className='p-1 flex flex-col space-y-1'>
				<span className='font-bold text-main-primary-border'>
					{new Date(data.updated_at).toLocaleString('id-ID', options)}
				</span>
				<button className='inline-flex items-center space-x-2 text-gray-700'>
					<FingerPrintIcon className='w-5' />
					<span>Pribadi</span>
				</button>
				<span className='text-xs text-gray-400 italic'>{data.id}</span>
			</td>
			<td className='border p-1'>
				<span className='font-bold'>{data.judul}</span>
				<br />
				<span>{data.perihal}</span>
			</td>
		</tr>
	);
}

export default ESignPribadi;
