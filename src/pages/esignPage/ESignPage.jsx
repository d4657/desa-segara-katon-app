import { LinkIcon, UserCircleIcon } from '@heroicons/react/outline';
import React, { useEffect, useState } from 'react';
import { Link, Outlet } from 'react-router-dom';
import Title from '../../components/static/Title';
import WarningContainer from '../../components/static/WarningContainer';
import useAuth from '../../hooks/useAuth';
import KelolaEsign from './component/KelolaEsign';
import PermintaanEsign from './component/PermintaanEsign';

function ESignPage(props) {
	const { user } = useAuth();

	return (
		<div>
			<Title text='Electronic Sign' />
			{!user.tandatangan ? (
				<div className='p-2 pb-0'>
					<WarningContainer className='h-14 inline-flex items-center w-full p-2'>
						<span>
							Pengguna yang belum memiliki tandatangan tidak diperkenankan untuk
							menggunakan fitur permintaan e-sign.
							<br />
							Untuk menggunakan fitur, silahkan kembali ke menu{' '}
							<Link
								to='/profil'
								className='text-main-acc inline-flex text-center'
							>
								<span>Profil</span>
								<UserCircleIcon className='w-4' />
							</Link>{' '}
							dan mengupload kredensial berupa tandatangan.
						</span>
					</WarningContainer>
				</div>
			) : null}
			<Outlet />
		</div>
	);
}

export default ESignPage;
