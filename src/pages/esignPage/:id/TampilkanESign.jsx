import {
	CalendarIcon,
	CheckCircleIcon,
	ChevronLeftIcon,
	XCircleIcon,
} from '@heroicons/react/outline';
import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useNavigate, useParams } from 'react-router-dom';
import { baseURL } from '../../../API/Api';
import UserApi from '../../../API/UserApi';
import UsersESignApi from '../../../API/UsersESignApi';
import ErrorMsg from '../../../components/forms/ErrorMsg';
import Loading from '../../../components/static/Loading';
import SmallLoading from '../../../components/static/SmallLoading';
import useAuth from '../../../hooks/useAuth';

function TampilkanESign(props) {
	const { id } = useParams();
	const [esign, setEsign] = useState({});
	const navigate = useNavigate();

	const [loading, setLoading] = useState(true);
	const [staffs, setStaffs] = useState([]);
	const [focus, setFocus] = useState(false);
	const [cariNama, setCariNama] = useState({ cari_nama: '' });
	const [type, setType] = useState();
	const [catatan_tambahan, setCatatanTambahan] = useState('');
	const { user } = useAuth();
	const [diterima, setditerima] = useState(false);
	const [showHapus, setShowHapus] = useState(false);

	function toggleHapus() {
		setShowHapus(!showHapus);
	}

	const [reqErr, setErrReq] = useState({});

	useEffect(() => {
		UsersESignApi.getUsersEsignId(id)
			.then((res) => {
				const payload = res.data.payload;
				setCariNama({ cari_nama: payload.penerima_simple.nama_lengkap });
				if (payload.id_pengirim === payload.id_penerima) {
					setType('pribadi');
				} else if (user.id === payload.id_penerima) {
					setType('diterima');
				} else if (user.id === payload.id_pengirim) {
					setType('dikirim');
				}

				setditerima(!!payload.waktu_diterima);
				setCatatanTambahan(payload.catatan_tambahan);
				setEsign(payload);
				setLoading(false);
			})
			.catch((err) => {
				navigate('/e-sign');
				setLoading(false);
			});
	}, [id]);

	useEffect(() => {
		reset({
			...esign,
			// penerima: esign.penerima_simple.nama_lengkap,
			// id_penerima: esign.penerima_simple.id,
			waktu_valid: new Date(esign.waktu_valid)
				.toLocaleString('en-GB')
				.substring(0, 10)
				.split('/')
				.reverse()
				.join('-'),
			waktu_diterima: esign.waktu_diterima
				? new Date(esign.waktu_diterima)
						.toLocaleString('en-GB')
						.substring(0, 10)
						.split('/')
						.reverse()
						.join('-')
				: '',
		});
	}, [esign]);

	const {
		register,
		handleSubmit,
		setValue,
		resetField,
		reset,
		getValues,

		formState: { errors },
	} = useForm({
		criteriaMode: 'all',
		defaultValues: {
			...esign,
		},
	});

	useEffect(() => {
		if (focus && cariNama.cari_nama) {
			UserApi.findStaffByName(cariNama).then((res) => {
				const payload = res.data.payload;
				setStaffs(payload);
			});
		}
	}, [cariNama, focus]);

	const [updateState, setUpdateState] = useState({
		loading: false,
		success: false,
		error: false,
	});

	function handleSimpan(data, e) {
		e.preventDefault();
		setUpdateState({
			...updateState,
			loading: true,
		});

		UsersESignApi.updateUserEsignById(data, id)
			.then(async (res) => {
				setEsign(res.data.payload);
				setUpdateState({
					...updateState,
					loading: false,
					success: true,
				});
				await new Promise((resolve) => setTimeout(resolve, 2000));
				setUpdateState({
					...updateState,
					success: false,
				});
			})
			.cath(async (err) => {
				setUpdateState({
					...updateState,
					loading: false,
					error: true,
				});
				await new Promise((resolve) => setTimeout(resolve, 2000));
				setUpdateState({
					...updateState,
					error: false,
				});
			});
	}

	function handleAcc() {
		UsersESignApi.acceptEsign({ catatan_tambahan }, id)
			.then((res) => {
				const payload = res.data.payload;
				setEsign(payload);
				setditerima(true);
			})
			.catch((err) => setErrReq(err.response.data.errors));
	}

	function handleReset() {
		reset({
			...esign,
			penerima: esign.penerima_simple.nama_lengkap,
			id_penerima: esign.penerima_simple.id,
			waktu_valid: new Date(esign.waktu_valid)
				.toLocaleString('en-GB')
				.substring(0, 10)
				.split('/')
				.reverse()
				.join('-'),

			waktu_diterima: esign.waktu_diterima
				? new Date(esign.waktu_diterima)
						.toLocaleString('en-GB')
						.substring(0, 10)
						.split('/')
						.reverse()
						.join('-')
				: '',
		});
	}

	function handleHapus() {
		UsersESignApi.deleteEsignById(id).then((res) => {
			navigate('/e-sign');
		});
	}

	if (loading === true) {
		return <Loading />;
	}

	return (
		<div className='m-2 border flex flex-col p-2 space-y-1 bg-white'>
			<div className='py-2 inline-flex items-center space-x-2 select-none'>
				<button
					onClick={() => navigate('/e-sign')}
					className='btn-primary'
					id='perm-ttd'
				>
					<ChevronLeftIcon className='w-5' />
				</button>
				<label
					htmlFor={'perm-ttd'}
					className='font-bold text-lg cursor-pointer hover:text-main-primary-border'
				>
					Rincian Esign
				</label>
			</div>
			<hr />

			<div className='gap-x-10 grid lg:grid-cols-2 grid-cols-1 '>
				<form onSubmit={handleSubmit(handleSimpan)} className='space-y-1 '>
					<div className='flex lg:flex-row flex-col '>
						<label className='py-1 w-36' htmlFor=''>
							Judul
						</label>
						<input
							disabled={diterima}
							{...register('judul', {
								required: 'Judul E-Sign harus terisi',
							})}
							placeholder='Masukan judul'
							type='text'
							className='input-primary flex-grow'
						/>
					</div>
					<div className='text-right'>
						<ErrorMsg>{errors.judul ? errors.judul.message : null}</ErrorMsg>
						<ErrorMsg>
							{reqErr.judul ? errors.judul.map((msg) => msg) : null}
						</ErrorMsg>
					</div>

					<div className='flex lg:flex-row flex-col '>
						<label className='py-1 w-36' htmlFor=''>
							Pengirim
						</label>
						<div className='flex flex-col flex-grow space-y-1 '>
							<div className='flex flex-col relative'>
								<input
									disabled
									value={esign.pengirim_simple.nama_lengkap}
									{...register('pengirim', {})}
									autoComplete='off'
									type='text'
									className='input-primary'
								/>
							</div>
						</div>
					</div>

					<div className='flex lg:flex-row flex-col '>
						<label className='py-1 w-36' htmlFor=''>
							Ditujukan Kepada
						</label>
						<div className='flex flex-col flex-grow space-y-1 '>
							<div className='flex flex-col relative'>
								<input
									disabled
									value={esign.penerima_simple.nama_lengkap}
									{...register('pengirim', {})}
									autoComplete='off'
									type='text'
									className='input-primary'
								/>
							</div>
						</div>
						<div className='text-right'>
							<ErrorMsg>
								{errors.penerima ? errors.penerima.message : null}
							</ErrorMsg>
							<ErrorMsg>
								{reqErr.id_penerima
									? errors.id_penerima.map((msg) => msg)
									: null}
							</ErrorMsg>
						</div>
					</div>

					<div className='flex lg:flex-row flex-col '>
						<label className='py-1 w-36' htmlFor=''>
							Perihal
						</label>
						<textarea
							disabled={diterima}
							{...register('perihal', {
								required: 'Perihal permintaan tandatangan harus terisi',
							})}
							placeholder='Masukan perihal'
							className='input-primary flex-grow'
						/>
					</div>
					<div className='text-right'>
						<ErrorMsg>
							{errors.perihal ? errors.perihal.message : null}
						</ErrorMsg>
						<ErrorMsg>
							{reqErr.id_perihal ? errors.id_perihal.map((msg) => msg) : null}
						</ErrorMsg>
					</div>

					<div className='flex lg:flex-row flex-col '>
						<label className='py-1 w-36' htmlFor=''>
							Tanggal
						</label>
						<input
							disabled={diterima}
							{...register('waktu_valid', {
								required: 'Tanggal valid E-Sign harus terisi',
							})}
							type='date'
							className='input-primary lg:w-72 w-full'
						/>
					</div>

					{diterima || type === 'diterima' ? null : (
						<div className='flex flex-row space-x-1 justify-end'>
							<div className='relative'>
								<button
									onClick={toggleHapus}
									type='button'
									className='btn-danger w-20'
								>
									Hapus
								</button>
								{showHapus ? (
									<div className='absolute right-0 w-32 border bg-white p-1 shadow flex flex-col gap-1 '>
										<span>Yakin untuk menghapus?</span>
										<div className='grid grid-cols-2 gap-1'>
											<button onClick={toggleHapus} className='btn-secondary'>
												Batal
											</button>
											<button onClick={handleHapus} className='btn-danger'>
												Ya
											</button>
										</div>
									</div>
								) : null}
							</div>
							<button
								type='button'
								onClick={handleReset}
								className='btn-secondary w-20'
							>
								Reset
							</button>
							<button type='submit' className='btn-primary w-20'>
								{updateState.loading ? (
									<SmallLoading />
								) : updateState.success ? (
									<CheckCircleIcon className='w-5 m-auto' />
								) : updateState.error ? (
									<XCircleIcon className='w-5 m-auto ' />
								) : (
									<span>Simpan</span>
								)}
							</button>
						</div>
					)}

					{type === 'pribadi' ? (
						<div className='flex flex-row space-x-1 justify-end'>
							<div className='relative'>
								<button
									onClick={toggleHapus}
									type='button'
									className='btn-danger w-20'
								>
									Hapus
								</button>
								{showHapus ? (
									<div className='absolute right-0 w-32 border bg-white p-1 shadow flex flex-col gap-1 '>
										<span>Yakin untuk menghapus?</span>
										<div className='grid grid-cols-2 gap-1'>
											<button onClick={toggleHapus} className='btn-secondary'>
												Batal
											</button>
											<button onClick={handleHapus} className='btn-danger'>
												Ya
											</button>
										</div>
									</div>
								) : null}
							</div>
						</div>
					) : null}
				</form>

				<div className='space-y-1'>
					{type !== 'pribadi' && type !== 'dikirim' ? (
						<div className='space-y-1'>
							<div className='flex lg:flex-row flex-col'>
								<label className='py-1 w-36' htmlFor=''>
									Catatan Tambahan
								</label>
								<div className='flex-grow'>
									<textarea
										disabled={diterima}
										value={catatan_tambahan}
										onChange={(e) => setCatatanTambahan(e.target.value)}
										className='input-primary w-full'
									/>
								</div>
							</div>

							<div className='flex flex-row space-x-1 justify-end'>
								{diterima ? (
									<button className='inline-flex items-center p-1 text-main-acc space-x-1 '>
										<CheckCircleIcon className='w-5' />
										<span>Diterima</span>
									</button>
								) : (
									<button
										type='button'
										onClick={handleAcc}
										className='btn-primary px-4'
									>
										Terima
									</button>
								)}
							</div>
							<hr />
						</div>
					) : null}

					<div className='flex lg:flex-row flex-col '>
						<label className='py-1 w-36' htmlFor=''>
							Tandatangan
						</label>
						<div className='border flex-grow border-gray-400 rounded h-32'>
							{diterima ? (
								<img
									src={baseURL + '/storage/e-sign/qr-sign/' + esign.id}
									alt='qr'
									className='h-full'
								/>
							) : null}
						</div>
					</div>

					<div className='flex lg:flex-row flex-col '>
						<label className='py-1 w-36' htmlFor=''>
							Tanggal diterima
						</label>

						<div className='flex flex-col flex-grow space-y-1'>
							<div className='flex flex-row'>
								<button
									type='button'
									className='btn-secondary w-20 flex-shrink-0 flex justify-center'
								>
									<CalendarIcon className='w-5' />
								</button>
								<input
									disabled
									value={esign.waktu_diterima}
									className='input-primary flex-grow'
								/>
							</div>
						</div>
					</div>

					<div className='flex lg:flex-row flex-col '>
						<label className='py-1 w-36' htmlFor=''>
							Download
						</label>
						<div className='flex flex-col flex-grow space-y-1'>
							<div className='flex flex-row'>
								<button
									type='button'
									className='btn-secondary w-20 flex-shrink-0'
									onClick={() => {
										if (diterima) {
											window.open(
												baseURL + '/storage/e-sign/qr/' + esign.id,
												'_blank'
											);
										}
									}}
								>
									Qr
								</button>
								<input
									value={
										esign.waktu_diterima
											? baseURL + '/storage/e-sign/qr/' + esign.id
											: ''
									}
									className='input-primary flex-grow'
								/>
							</div>
							<div className='flex flex-row '>
								<button
									type='button'
									className='btn-secondary w-20 flex-shrink-0'
									onClick={() => {
										if (diterima) {
											window.open(
												baseURL + '/storage/e-sign/qr-sign/' + esign.id,
												'_blank'
											);
										}
									}}
								>
									Qr + Sign
								</button>
								<input
									value={
										esign.waktu_diterima
											? baseURL + '/storage/e-sign/qr-sign/' + esign.id
											: ''
									}
									className='input-primary flex-grow'
								/>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
}

export default TampilkanESign;
