import React from 'react';
import useAuth from '../../hooks/useAuth';
import KelolaEsign from './component/KelolaEsign';
import PermintaanEsign from './component/PermintaanEsign';

function ESignPageIndex(props) {
	const { user } = useAuth();
	console.log(user)
	return (
		<div>
			{user.tandatangan ? <PermintaanEsign /> : null}
			<KelolaEsign />
		</div>
	);
}

export default ESignPageIndex;
