import { CameraIcon, UserCircleIcon } from '@heroicons/react/outline';
import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { Api, baseURL } from '../../API/Api';
import ImageUploadModal from '../../components/modals/ImageUploadModal';
import Title from '../../components/static/Title';
import WarningContainer from '../../components/static/WarningContainer';
import useAuth from '../../hooks/useAuth';
import Akun from './component/Akun';
import IdentitasDiri from './component/IdentitasDiri';
import Keamanan from './component/Keamanan';
import Tandatangan from './component/Tandatangan';

function ProfilePage(props) {
	const { user } = useAuth();
	const [showPhoto, setShowPhoto] = useState(false);

	return (
		<div className=''>
			{showPhoto ? (
				<ImageUploadModal showPhoto={showPhoto} setShowPhoto={setShowPhoto} />
			) : null}
			<Title text={'Selamat Datang ' + user.nama_lengkap} />

			{!user.tandatangan || !user.foto || !user.no_hp ? (
				<div className='p-2 pb-0'>
					<WarningContainer className='h-14 inline-flex items-center w-full p-2'>
						<span>
							Pastikan untuk melengkapi profil jika ingin menggunakan
							fitur-fitur yang ada
							<br />
							Beberapa fitur mungkin tidak akan berfungsi jika foto, identitas
							diri, akun atau tandatangan belum terisi
						</span>
					</WarningContainer>
				</div>
			) : null}

			<div className='m-2 flex flex-col border space-y-2 p-2 bg-white'>
				<div className='py-2'>
					<span className='font-bold text-lg'>Profil Penguna</span>
				</div>
				<hr />

				<div className=' border flex flex-col justify-evenly lg:flex-row flex-grow '>
					<div className=' p-2 w-full justify-center flex lg:w-96 '>
						<div className='items-center space-y-1 '>
							<div className='w-80 h-80 overflow-hidden grid rounded border border-main-primary-border '>
								{user.foto ? (
									<img
										src={baseURL + '/storage/foto/' + user.foto}
										className='w-80'
										alt='foto_profil'
									></img>
								) : (
									<UserCircleIcon className='text-gray-200 w-72 m-auto' />
								)}
							</div>

							<div className='flex flex-row '>
								<button
									onClick={() => setShowPhoto(!showPhoto)}
									className='btn-secondary flex-grow inline-flex justify-center items-center space-x-2'
								>
									<CameraIcon className='w-5' />
									<span>Perbaharui foto</span>
								</button>
							</div>
						</div>
					</div>

					<div className=' grid lg:grid-cols-2 md:grid-cols-1 gap-4 p-2 flex-grow '>
						<IdentitasDiri user={user} />

						<Akun user={user} />

						<Keamanan user={user} />

						<Tandatangan user={user} />
					</div>
				</div>
			</div>
		</div>
	);
}

export default ProfilePage;
