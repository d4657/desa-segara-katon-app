import { CameraIcon, FingerPrintIcon } from '@heroicons/react/outline';
import React, { useState } from 'react';
import { baseURL } from '../../../API/Api';
import SignUploadModal from '../../../components/modals/SignUploadModal';

function Tandatangan(props) {
	const { user } = props;
	const [showPhoto, setShowPhoto] = useState(false);
	return (
		<div className='space-y-1'>
			{showPhoto ? (
				<SignUploadModal showPhoto={showPhoto} setShowPhoto={setShowPhoto} />
			) : null}

			<span className='font-bold pt-2'>Kredensial</span>
			<hr />

			<div className='flex flex-col lg:flex-row'>
				<label className='w-40 py-1' htmlFor=''>
					Tandatangan
				</label>
				<div className='space-y-1 w-full'>
					<div className='w-full h-40 border flex justify-center'>
						{user.tandatangan ? (
							<img
								className='h-full '
								src={baseURL + '/storage/tandatangan/' + user.tandatangan}
								alt='tandatangan'
							/>
						) : (
							<FingerPrintIcon className='w-10 text-gray-400' />
						)}
					</div>
					<button
						onClick={() => setShowPhoto(!showPhoto)}
						className='btn-secondary w-full inline-flex justify-center items-center space-x-2'
					>
						<CameraIcon className='w-5' />
						<span>Perbaharui tandatangan</span>
					</button>
				</div>
			</div>
		</div>
	);
}

export default Tandatangan;
