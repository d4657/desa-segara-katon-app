import { CheckCircleIcon, XCircleIcon } from '@heroicons/react/outline';
import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import UserApi from '../../../API/UserApi';
import ErrorMsg from '../../../components/forms/ErrorMsg';
import SmallLoading from '../../../components/static/SmallLoading';
import useAuth from '../../../hooks/useAuth';

function IdentitasDiri(props) {
	const [loading, setLoading] = useState(false);
	const [success, setSuccess] = useState(false);
	const [error, setError] = useState(false);

	const [reqErr, setReqErr] = useState({});

	const { user, setUser } = useAuth();

	const {
		register,
		handleSubmit,
		watch,
		formState: { errors },
		reset,
	} = useForm({
		defaultValues: props.user,
		criteriaMode: 'all',
	});

	function submitIdentity(data, e) {
		e.preventDefault();
		setLoading(true);
		setReqErr({});
		UserApi.updateBio(data)
			.then(async (res) => {
				setUser(res.data.payload);
				setLoading(false);
				setSuccess(true);
				await new Promise((resolve) => setTimeout(resolve, 2000));
				setSuccess(false);
			})
			.catch(async (err) => {
				setReqErr(err.response.data.errors);
				setError(true);
				setLoading(false);
				await new Promise((resolve) => setTimeout(resolve, 2000));
				setError(false);
			});
	}

	return (
		<form onSubmit={handleSubmit(submitIdentity)} className='space-y-1'>
			<span className='font-bold'>Identitas Diri</span>
			<hr />
			<div className='flex flex-col'>
				<div className='flex flex-col lg:flex-row'>
					<label className='w-32 py-1' htmlFor=''>
						Nama Lengkap
					</label>
					<input
						{...register('nama_lengkap', {
							required: 'Nama lengkap harus terisi',
							minLength: {
								value: 3,
								message: 'Nama Lengkap minimal terdiri dari 6 karakter',
							},
						})}
						className='flex-grow input-primary'
						placeholder='Masukan nama lengkap'
						type='text'
					/>
				</div>
				<div className='text-right'>
					<ErrorMsg>
						{errors.nama_lengkap ? errors.nama_lengkap.message : null}
					</ErrorMsg>
					<ErrorMsg>
						{reqErr.nama_lengkap ? reqErr.nama_lengkap.map((msg) => msg) : null}
					</ErrorMsg>
				</div>
			</div>

			{user.level_pengguna < 3 ? (
				<div className='flex flex-col'>
					<div className='flex flex-col lg:flex-row'>
						<label className='w-32' htmlFor=''>
							Jabatan
						</label>
						<input
							{...register('jabatan', {
								required: 'Jabatan harus terisi',
							})}
							placeholder='Masukan jabatan'
							className='flex-grow input-primary'
							type='text'
						/>
					</div>
					<div className='text-right'>
						<ErrorMsg>
							{errors.jabatan ? errors.jabatan.message : null}
						</ErrorMsg>
						<ErrorMsg>
							{reqErr.jabatan ? reqErr.jabatan.map((msg) => msg) : null}
						</ErrorMsg>
					</div>
				</div>
			) : null}

			<div className='flex flex-col'>
				<div className='flex flex-col lg:flex-row'>
					<label className='w-32 py-1' htmlFor=''>
						Alamat
					</label>
					<textarea
						{...register('alamat', {
							required: 'alamat harus terisi',
						})}
						placeholder='Masukan alamat'
						className='flex-grow input-primary'
						type='text'
					/>
				</div>
				<div className='text-right'>
					<ErrorMsg>{errors.alamat ? errors.alamat.message : null}</ErrorMsg>
					<ErrorMsg>
						{reqErr.alamat ? reqErr.alamat.map((msg) => msg) : null}
					</ErrorMsg>
				</div>
			</div>

			<div className='flex flex-col lg:flex-row '>
				<label className='w-32 hidden lg:inline  ' htmlFor=''></label>
				<div className='flex-grow  flex flex-row space-x-1'>
					<button
						type='button'
						onClick={() => reset(props.user)}
						className='btn-secondary w-full  '
					>
						Batal
					</button>

					<button
						disabled={loading}
						type='submit'
						className='btn-primary w-full '
					>
						{loading ? (
							<SmallLoading />
						) : success ? (
							<CheckCircleIcon className='w-5 m-auto' />
						) : error ? (
							<XCircleIcon className='w-5 m-auto' />
						) : (
							<span>Simpan</span>
						)}
					</button>
				</div>
			</div>
		</form>
	);
}

export default IdentitasDiri;
