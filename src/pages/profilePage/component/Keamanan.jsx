import { CheckCircleIcon, EyeIcon, EyeOffIcon, XCircleIcon } from '@heroicons/react/outline';
import React, { useRef, useState } from 'react';
import { useForm } from 'react-hook-form';
import UserApi from '../../../API/UserApi';
import ErrorMsg from '../../../components/forms/ErrorMsg';
import Loading from '../../../components/static/Loading';
import SmallLoading from '../../../components/static/SmallLoading';

function Keamanan(props) {
	// form state
	const [loading, setLoading] = useState(false);
	const [success, setSuccess] = useState(false);
	const [error, setError] = useState(false);

	const [reqErr, setReqErr] = useState({});

	const [passShown, setPassShown] = useState(false);
	const [newPassShown, setNewPassShown] = useState(false);

	const {
		register,
		handleSubmit,
		watch,
		formState: { errors },
		reset,
	} = useForm({
		criteriaMode: 'all',
	});

	function submitPassword(data, e) {
		e.preventDefault();
		setLoading(true);
		setReqErr({});
		UserApi.updatePassword(data)
			.then(async (res) => {
				setLoading(false);
				setSuccess(true);
				reset()
				await new Promise((resolve) => setTimeout(resolve, 2000));
				setSuccess(false);
			})
			.catch(async (err) => {
				console.log(err.response)
				setReqErr(err.response.data.errors);
				setError(true);
				setLoading(false);
				await new Promise((resolve) => setTimeout(resolve, 2000));
				setError(false);
			});
	}

	const password_baru = useRef({});
	password_baru.current = watch('password_baru', '');

	return (
		<form onSubmit={handleSubmit(submitPassword)} className='space-y-1'>
			<span className='font-bold pt-2'>Keamanan</span>
			<hr />

			<div className='flex flex-col'>
				<div className='flex flex-col lg:flex-row'>
					<label className='w-32 py-1' htmlFor=''>
						Password Lama
					</label>
					<div className='flex-grow flex flex-row items-center'>
						<input
							{...register('password_lama', {
								required: 'Password harus terisi',
							})}
							placeholder='●●●●●●●●●'
							className='flex-grow input-primary'
							type={passShown ? 'text' : 'password'}
						/>
						<button
							type='button'
							onClick={() => setPassShown(!passShown)}
							className='btn-secondary border-gray-400 rounded-none'
						>
							{passShown ? (
								<EyeIcon className='w-5' />
							) : (
								<EyeOffIcon className='w-5' />
							)}
						</button>
					</div>
				</div>
				<div className='text-right'>
					<ErrorMsg>
						{errors.password_lama ? errors.password_lama.message : null}
					</ErrorMsg>
					<ErrorMsg>
						{reqErr.password_lama
							? reqErr.password_lama.map((msg) => msg)
							: null}
					</ErrorMsg>
				</div>
			</div>

			<div className='flex flex-col'>
				<div className='flex flex-col lg:flex-row'>
					<label className='w-32 py-1' htmlFor=''>
						Password Baru
					</label>
					<div className='flex-grow flex flex-row items-center'>
						<input
							{...register('password_baru', {
								required: 'Password baru harus terisi',
								pattern: {
									value: /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$/,
									message: 'Format password tidak sesuai',
								},
							})}
							placeholder='Masukan password baru'
							className='flex-grow input-primary'
							type={newPassShown ? 'text' : 'password'}
						/>
						<button
							type='button'
							onClick={() => setNewPassShown(!newPassShown)}
							className='btn-secondary border-gray-400 rounded-none'
						>
							{newPassShown ? (
								<EyeIcon className='w-5' />
							) : (
								<EyeOffIcon className='w-5' />
							)}
						</button>
					</div>
				</div>
				<div className='text-right'>
					<ErrorMsg>
						{errors.password_baru ? errors.password_baru.message : null}
					</ErrorMsg>
					<ErrorMsg>
						{reqErr.password_baru
							? reqErr.password_baru.map((msg) => msg)
							: null}
					</ErrorMsg>
				</div>
			</div>

			<div className='flex flex-col'>
				<div className='flex flex-col lg:flex-row'>
					<label className='w-32' htmlFor=''></label>
					<div className='flex-grow flex flex-row items-center'>
						<input
							{...register('password_baru_confirmation', {
								validate: (value) =>
									value === password_baru.current || 'Password tidak sama',
							})}
							placeholder='Konfirmasi password baru'
							className='w-full input-primary'
							type={newPassShown ? 'text' : 'password'}
						/>
						<button
							type='button'
							onClick={() => setNewPassShown(!newPassShown)}
							className='btn-secondary border-gray-400 rounded-none'
						>
							{newPassShown ? (
								<EyeIcon className='w-5' />
							) : (
								<EyeOffIcon className='w-5' />
							)}
						</button>
					</div>
				</div>
				<div className='text-right'>
					<ErrorMsg>
						{errors.password_baru_confirmation
							? errors.password_baru_confirmation.message
							: null}
					</ErrorMsg>
					<ErrorMsg>
						{reqErr.password_baru_confirmation
							? reqErr.password_baru_confirmation.map((msg) => msg)
							: null}
					</ErrorMsg>
				</div>
			</div>

			<div className='flex flex-col lg:flex-row '>
				<label className='w-32 hidden lg:inline' htmlFor=''></label>
				<div className='flex-grow flex space-x-1'>
					<button type='reset' className='btn-secondary w-full '>
						Batal
					</button>
					<button disabled={loading} type='submit' className='btn-primary w-full'>
						{loading ? (
							<SmallLoading />
						) : success ? (
							<CheckCircleIcon className='w-5 m-auto' />
						) : error ? (
							<XCircleIcon className='w-5 m-auto text-main-danger' />
						) : (
							<span>Simpan</span>
						)}
					</button>
				</div>
			</div>
		</form>
	);
}

export default Keamanan;
