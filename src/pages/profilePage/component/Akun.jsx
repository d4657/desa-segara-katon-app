import {
	CheckCircleIcon,
	EyeIcon,
	EyeOffIcon,
	XCircleIcon,
} from '@heroicons/react/outline';
import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import UserApi from '../../../API/UserApi';
import ErrorMsg from '../../../components/forms/ErrorMsg';
import SmallLoading from '../../../components/static/SmallLoading';
import useAuth from '../../../hooks/useAuth';

function Akun(props) {
	const [loading, setLoading] = useState(false);
	const [success, setSuccess] = useState(false);
	const [error, setError] = useState(false);

	const [reqErr, setReqErr] = useState({});

	const { user, setUser } = useAuth();
	const [idShown, setIdShown] = useState(false);
	const {
		register,
		handleSubmit,
		watch,
		formState: { errors },
		reset,
	} = useForm({
		criteriaMode: 'all',
		defaultValues: props.user,
	});

	function saveAccount(data, e) {
		e.preventDefault();
		setLoading(true);
		setReqErr({});
		UserApi.updateAccount(data)
			.then(async (res) => {
				setUser(res.data.payload);
				setLoading(false);
				setSuccess(true);
				await new Promise((resolve) => setTimeout(resolve, 2000));
				setSuccess(false);
			})
			.catch(async (err) => {
				setReqErr(err.response.data.errors);
				setError(true);
				setLoading(false);
				await new Promise((resolve) => setTimeout(resolve, 2000));
				setError(false);
			});
	}

	return (
		<form onSubmit={handleSubmit(saveAccount)} className='space-y-1'>
			<span className='font-bold'>Akun</span>
			<hr />

			<div className='flex flex-col lg:flex-row	'>
				<label className='w-32 py-1'>Id Pengguna</label>
				<div className='flex flex-row flex-grow'>
					<input
						disabled
						className='input-primary flex-grow italic text-gray-400'
						value={props.user.id}
						type={idShown ? 'text' : 'password'}
					/>
					<button
						type='button'
						onClick={() => setIdShown(!idShown)}
						className='btn-secondary border-gray-400 rounded-none'
					>
						{idShown ? (
							<EyeIcon className='w-5' />
						) : (
							<EyeOffIcon className='w-5' />
						)}
					</button>
				</div>
			</div>
			<div className='flex flex-col'>
				<div className='flex flex-col lg:flex-row'>
					<label className='w-32 py-1' htmlFor=''>
						Username
					</label>
					<input
						{...register('username', {
							required: 'Username harus terisi',
							minLength: {
								value: 6,
								message: 'Username minimal terdiri dari 6 karakter',
							},
							maxLength: {
								value: 20,
								message: 'Username maksimial terdiri dari 20 karakter',
							},
							pattern: {
								value:
									/^(?=.{6,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$/,
								message: 'Format username tidak valid',
							},
						})}
						placeholder='Masukan username'
						className='flex-grow input-primary'
						type='text'
					/>
				</div>
				<div className='text-right'>
					<ErrorMsg>
						{errors.username ? errors.username.message : null}
					</ErrorMsg>
					<ErrorMsg>
						{reqErr.username ? reqErr.username.map((msg) => msg) : null}
					</ErrorMsg>
				</div>
			</div>

			<div className='flex flex-col'>
				<div className='flex flex-col lg:flex-row'>
					<label className='w-32 py-1' htmlFor=''>
						Email
					</label>
					<input
						{...register('email', {
							required: 'Email harus terisi',
							pattern: {
								value:
									/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
								message: 'Format email tidak valid',
							},
						})}
						placeholder='Masukan email'
						className='flex-grow input-primary'
						type='text'
					/>
				</div>
				<div className='text-right'>
					<ErrorMsg>{errors.email ? errors.email.message : null}</ErrorMsg>
					<ErrorMsg>
						{reqErr.email ? reqErr.email.map((msg) => msg) : null}
					</ErrorMsg>
				</div>
			</div>

			<div className='flex flex-col'>
				<div className='flex flex-col lg:flex-row'>
					<label className='w-32 py-1' htmlFor=''>
						No. Hp
					</label>
					<input
						{...register('no_hp', {
							required: 'Nomor Hp harus terisi',
							pattern: {
								value: /^[0-9]*$/,
								message: 'Format nomor Hp tidak valid',
							},
						})}
						placeholder='Masukan nomor Hp'
						className='flex-grow input-primary'
						type='text'
					/>
				</div>
				<div className='text-right'>
					<ErrorMsg>
						<ErrorMsg>{errors.no_hp ? errors.no_hp.message : null}</ErrorMsg>
					</ErrorMsg>
					<ErrorMsg>
						{reqErr.no_hp ? reqErr.no_hp.map((msg) => msg) : null}
					</ErrorMsg>
				</div>
			</div>

			<div className='flex flex-col lg:flex-row '>
				<label className='w-32 hidden lg:inline' htmlFor=''></label>
				<div className='flex-grow flex space-x-1'>
					<button
						type='button'
						onClick={() => reset(props.user)}
						className='btn-secondary w-full  '
					>
						Batal
					</button>
					<button
						disabled={loading}
						type='submit'
						className='btn-primary w-full '
					>
						{loading ? (
							<SmallLoading />
						) : success ? (
							<CheckCircleIcon className='w-5 m-auto' />
						) : error ? (
							<XCircleIcon className='w-5 m-auto text-main-danger' />
						) : (
							<span>Simpan</span>
						)}
					</button>
				</div>
			</div>
		</form>
	);
}

export default Akun;
