import {
	ChevronLeftIcon,
	UserCircleIcon
} from '@heroicons/react/outline';
import React, { useEffect, useState } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { baseURL } from '../../../API/Api';
import UserApi from '../../../API/UserApi';
import useAuth from '../../../hooks/useAuth';
import KeamananPengguna from './component/KeamananPengguna';

function ShowUser(props) {
	const { id } = useParams();
	const { user } = useAuth();
	const [currentUser, setCurrentUser] = useState({});
	const navigate = useNavigate();
	const [reload, setReload] = useState();

	const [showDel, setShowDel] = useState(false);
	const [showUp, setShowUp] = useState(false);
	const [selectedLevel, setSelectedLevel] = useState();

	function toggleDel() {
		setShowDel(!showDel);
	}

	function toggleUp() {
		setShowUp(!showUp);
	}

	useEffect(() => {
		UserApi.getUserById(id)
			.then((res) => {
				const payload = res.data.payload;
				setSelectedLevel(payload.level_pengguna);
				setCurrentUser(payload);
				if (!payload) {
					navigate('/pengguna');
				}
			})
			.catch((err) => {
				if (err) {
					navigate('/pengguna');
				}
			});
		return () => {};
	}, [reload]);

	function handleLevelChange() {
		// console.log(e.target.value)
		UserApi.upgradeAccess({ level: selectedLevel }, id)
			.then((res) => {
				setCurrentUser(res.data.payload);
				setShowUp(!showUp)
				setReload(!reload);
			})
			.catch((err) => {});
	}

	function handleDeleteUser(){
		UserApi.deleteUserById(id)
		.then(
			(res)=>{
				navigate('/pengguna')
			}
		)
	}

	return (
		<div className='m-2 flex flex-col border space-y-2 p-2 bg-white'>
			<div className='py-2 flex flex-row space-x-1 items-center'>
				<Link className='btn-primary' to='/pengguna'>
					<ChevronLeftIcon className='w-5' />
				</Link>
				<span className='font-bold text-lg'>Profil Pengguna</span>
			</div>
			<hr />

			<div className=' flex flex-col justify-evenly md:flex-row flex-grow '>
				<div className='space-y-1 p-2 flex flex-row justify-center  '>
					<div className='w-96 h-96 overflow-hidden rounded border border-main-primary-border flex '>
						{currentUser.foto ? (
							<img
								src={baseURL + '/storage/foto/' + currentUser.foto}
								className='w-96'
								alt='foto_profil'
							></img>
						) : (
							<UserCircleIcon className='text-gray-200 w-72 m-auto' />
						)}
					</div>
				</div>

				<div className='p-2 flex-grow space-y-1'>
					<div>
						<span className='font-bold'>Identitas Pengguna</span>
					</div>
					<hr />

					<div className='flex flex-col'>
						<div className='flex flex-col lg:flex-row'>
							<label className='w-32 py-1' htmlFor=''>
								Username
							</label>
							<input
								disabled
								value={currentUser.username}
								className='flex-grow input-primary'
								type='text'
							/>
						</div>
					</div>

					<div className='flex flex-col'>
						<div className='flex flex-col lg:flex-row'>
							<label className='w-32 py-1' htmlFor=''>
								Nama Lengkap
							</label>
							<input
								disabled
								className='flex-grow input-primary'
								value={currentUser.nama_lengkap}
								type='text'
							/>
						</div>
					</div>

					<div className='flex flex-col'>
						<div className='flex flex-col lg:flex-row'>
							<label className='w-32' htmlFor=''>
								Jabatan
							</label>
							<input
								disabled
								value={currentUser.jabatan}
								className='flex-grow input-primary'
								type='text'
							/>
						</div>
					</div>

					<div className='flex flex-col'>
						<div className='flex flex-col lg:flex-row'>
							<label className='w-32 py-1' htmlFor=''>
								Alamat
							</label>
							<textarea
								disabled
								value={currentUser.alamat}
								className='flex-grow input-primary'
								type='text'
							/>
						</div>
					</div>
					{user.level_pengguna < 0 ||
					(user.level_pengguna === 0 && currentUser.level_pengguna > 0) ? (
						<>
							<KeamananPengguna id={id} />
							<div className='space-y-1'>
								<div>
									<span className='font-bold'>Zona admin</span>
								</div>
								<hr />

								<div className='space-y-1'>
									<div className='flex flex-col lg:flex-row'>
										<label className='w-32 py-1' htmlFor=''>
											Level Pengguna
										</label>
										<div className='flex flex-col relative'>
											<div
												disabled
												className='w-52 grid grid-flow-col'
												type='text'
											>
												<button
													value={0}
													onClick={(e) => {
														setSelectedLevel(e.target.value);
														setShowUp(true);
													}}
													className={
														'' +
														(currentUser.level_pengguna <= 0
															? 'btn-primary'
															: 'btn-secondary ')
													}
												>
													0
												</button>
												<button
													value={1}
													onClick={(e) => {
														setSelectedLevel(e.target.value);
														setShowUp(true);
													}}
													className={
														'' +
														(currentUser.level_pengguna === 1
															? 'btn-primary'
															: 'btn-secondary ')
													}
												>
													1
												</button>
												<button
													value={2}
													onClick={(e) => {
														setSelectedLevel(e.target.value);
														setShowUp(true);
													}}
													className={
														'' +
														(currentUser.level_pengguna === 2
															? 'btn-primary'
															: 'btn-secondary ')
													}
												>
													2
												</button>
												<button
													value={3}
													onClick={(e) => {
														setSelectedLevel(e.target.value);
														setShowUp(true);
													}}
													className={
														'' +
														(currentUser.level_pengguna === 3
															? 'btn-primary'
															: 'btn-secondary ')
													}
												>
													3
												</button>
											</div>
											{showUp ? (
													<div className='absolute w-full border bg-white p-1 shadow flex flex-col gap-1 z-10 top-8 '>
														<span>Update user ke level {selectedLevel}?</span>
														<div className='grid grid-cols-2 gap-1'>
															<button
																onClick={toggleUp}
																className='btn-secondary'
															>
																Batal
															</button>
															<button
																onClick={handleLevelChange}
																className='btn-primary'
															>
																Ya
															</button>
														</div>
													</div>
												) : null}
										</div>
									</div>

									<div className='flex flex-col lg:flex-row'>
										<label className='w-32 py-1' htmlFor=''>
											Hapus Pengguna
										</label>

										<div className='relative'>
											<button onClick={toggleDel} className='btn-danger w-52'>
												Hapus
											</button>
											{showDel ? (
												<div className='absolute w-full border bg-white p-1 shadow flex flex-col gap-1 '>
													<span>Yakin untuk menghapus?</span>
													<div className='grid grid-cols-2 gap-1'>
														<button
															onClick={toggleDel}
															className='btn-secondary'
														>
															Batal
														</button>
														<button onClick={handleDeleteUser} className='btn-danger'>
															Ya
														</button>
													</div>
												</div>
											) : null}
										</div>
									</div>
								</div>
							</div>
						</>
					) : null}
				</div>
			</div>
		</div>
	);
}

export default ShowUser;
