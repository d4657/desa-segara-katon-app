import React, {  } from 'react';
import { Outlet } from 'react-router-dom';
import Title from '../../components/static/Title';

function PenggunaPage(props) {

	return (
		<div>
			<Title text='Pengguna' />
      <Outlet/>
		</div>
	);
}

export default PenggunaPage;
