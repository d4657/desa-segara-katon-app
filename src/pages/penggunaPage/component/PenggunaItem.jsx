import {
	LocationMarkerIcon,
	PhoneIcon,
	UserCircleIcon,
} from '@heroicons/react/outline';
import React from 'react';
import { Link } from 'react-router-dom';
import { baseURL } from '../../../API/Api';

function PenggunaItem(props) {
	const { data } = props;

	return (
		<Link
			to={data.id}
			className='group w-full flex flex-row space-x-2 text-gray-700 '
		>
			{/* user profile */}
			<div className='w-16 h-16 border rounded grid flex-shrink-0 overflow-hidden'>
				{data.foto ? (
					<img
						src={baseURL + '/storage/foto/' + data.foto}
						className='w-16'
						alt='foto_profil'
					></img>
				) : (
					<UserCircleIcon className='text-gray-200 w-12 m-auto' />
				)}
			</div>
			{/* user quick bio */}
			<div className='flex-grow flex flex-col max-h-16 overflow-y-auto'>
				<span className='font-bold cursor-pointer group-hover:underline group-hover:text-main-primary-border '>
					{data.nama_lengkap}
				</span>
				<hr />
				<span className='inline-flex items-start space-x-2 text-s text-gray-500'>
					<PhoneIcon className='w-3 pt-1' />
					<span>{data.no_hp}</span>
				</span>
				<span className='inline-flex items-start space-x-2 text-s text-gray-500'>
					<LocationMarkerIcon className='w-3 pt-1 flex-shrink-0' />
					<span>{data.alamat}</span>
				</span>
			</div>
		</Link>
	);
}

export default PenggunaItem;
