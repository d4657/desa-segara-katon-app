import React from 'react';
import PenggunaGrup from './PenggunaGrup';

function KelolaPengguna(props) {
  return (
    <div>
      <PenggunaGrup
				user='admin'
			>
				Admin
			</PenggunaGrup>
			<PenggunaGrup
				user='staff'
			>
				Staff
			</PenggunaGrup>
			<PenggunaGrup
				user='editor'
			>
				Editor
			</PenggunaGrup>
			<PenggunaGrup
				user='penduduk'
			>
				Pengguna Biasa
			</PenggunaGrup>
    </div>
  );
}

export default KelolaPengguna;