import {
	ChevronLeftIcon,
	ChevronRightIcon,
	MinusIcon,
	PlusIcon,
	SearchIcon
} from '@heroicons/react/outline';
import debounce from 'lodash.debounce';
import React, { useEffect, useRef, useState } from 'react';
import UserApi from '../../../API/UserApi';
import Loading from '../../../components/static/Loading';
import PenggunaItem from './PenggunaItem';

function PenggunaGrup(props) {
	const { user } = props;
	const [shown, setShown] = useState(false);
	const [users, setUsers] = useState();
	const [page, setPage] = useState({});
	const [query, setQuery] = useState({
		perpage: 18,
		page: 1,
		cari_nama: '',
	});
	const [valueSearch, setValueSearch] = useState('');

	const [loading, setLoading] = useState(true);

	function toggle() {
		setShown(!shown);
	}

	useEffect(() => {
		UserApi.getUserByType(user, query)
			.then((res) => {
				setUsers(res.data.payload.data);
				setPage({
					current_page: res.data.payload.current_page,
					last_page: res.data.payload.last_page,
				});
				setLoading(false);
			})
			.catch((res) => {});
	}, [query, user]);

	function nextPage() {
		if (page.current_page < page.last_page) {
			setQuery({ ...query, page: query.page + 1 });
		}
	}

	function prevPage() {
		if (page.current_page > 1) {
			setQuery({ ...query, page: query.page - 1 });
		}
	}

	const debouncedSave = useRef(
		debounce((nextValue) => setQuery({ ...query, cari_nama: nextValue }), 1000)
	).current;

	function handleChangeSearchChange(event) {
		const { value: nextValue } = event.target;
		setValueSearch(nextValue);
		debouncedSave(nextValue);
	}

	return (
		<div className='m-2 flex flex-col border space-y-2 p-2 bg-white'>
			<div className='py-2 inline-flex items-center space-x-2'>
				<button onClick={toggle} className='btn-primary' id={props.user}>
					{shown ? <MinusIcon className='w-5' /> : <PlusIcon className='w-5' />}
				</button>
				<label
					htmlFor={props.user}
					className='font-bold text-lg cursor-pointer hover:text-main-primary-border'
				>
					{props.children}
				</label>
			</div>

			{shown ? (
				<div className='flex flex-col space-y-1'>
					<div className='flex gap-1 flex-row justify-between'>
						<div className='inline-flex '>
							<dic className='inline-flex items-center border border-gray-400 w-10 border-r-0 '>
								<SearchIcon className='w-5 m-auto' />
							</dic>
							<input
								type='text'
								className='input-primary lg:w-72 w-full'
								placeholder='Cari Pengguna...'
								value={valueSearch}
								onChange={handleChangeSearchChange}
							/>
						</div>
						<div className='space-x-1 inline-flex items-center '>
							<button
								onClick={prevPage}
								disabled={page.current_page <= 1}
								className='btn-secondary px-1 md:px-4'
							>
								<ChevronLeftIcon className='w-5' />
							</button>
							<span>
								{page.current_page}/{page.last_page}
							</span>
							<button
								onClick={nextPage}
								disabled={page.current_page >= page.last_page}
								className='btn-secondary px-1 md:px-4'
							>
								<ChevronRightIcon className='w-5' />
							</button>
						</div>
					</div>
					<hr />
					<div
						className={
							loading
								? 'grid-cols-1'
								: 'grid lg:grid-cols-3 md:grid-cols-2 grid-cols-1 gap-2 '
						}
					>
						{loading ? (
							<Loading />
						) : (
							users.map((data, index) => (
								<div
									key={index}
									className='border p-1 hover:border-main-primary-border hover:bg-main-primary hover:bg-opacity-5 rounded  '
								>
									<PenggunaItem data={data} />
								</div>
							))
						)}
					</div>
				</div>
			) : null}
		</div>
	);
}

export default PenggunaGrup;
