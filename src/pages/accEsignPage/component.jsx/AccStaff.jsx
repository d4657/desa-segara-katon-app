import { ChevronDoubleRightIcon } from '@heroicons/react/outline';
import React from 'react';
import { baseURL } from '../../../API/Api';
import Title from '../../../components/static/Title';

function AccStaff(props) {
	const { payload } = props;
	const options1 = {
		weekday: 'long',
		year: 'numeric',
		month: 'short',
		day: 'numeric',
	};

	const options = {
		weekday: 'long',
		year: 'numeric',
		month: 'short',
		day: 'numeric',
		hour: 'numeric',
		minute: 'numeric',
	};

	return (
		<div className='border bg-white lg:w-1/2 md:w-3/4 w-full m-auto p-2 flex flex-col gap-1'>
			<Title text='ELECTRONIC SIGNATURE' className='text-center w-full' />

			<div className='flex flex-row justify-center items-center space-x-1 '>
				<div className='w-72 p-2 space-y-1 rounded overflow-hidden border-main-primary border'>
					<img
						className='w-full rounded overflow-hidden '
						alt='user_img'
						src={baseURL + '/storage/foto/' + payload.pengirim_simple.foto}
					/>
					<div className='text-center font-bold text-xl'>
						<span>{payload.pengirim_simple.nama_lengkap}</span>
					</div>
				</div>
				<div>
					<ChevronDoubleRightIcon className='w-7 md:w-10 text text-main-primary' />
				</div>

				<div className='w-72 p-2 space-y-1 rounded overflow-hidden border-main-primary border'>
					<img
						className='w-full rounded overflow-hidden '
						alt='user_img'
						src={baseURL + '/storage/foto/' + payload.penerima_simple.foto}
					/>
					<div className='text-center font-bold text-xl'>
						<span>{payload.penerima_simple.nama_lengkap}</span>
					</div>
				</div>
			</div>

			<hr />
			<div className='flex flex-col lg:flex-row lg:items-center '>
				<label className='w-32'>Judul</label>
				<input
					value={payload.judul}
					className='input-primary flex-grow '
					disabled
				/>
			</div>

			<div className='flex flex-col lg:flex-row '>
				<label className='w-32 pt-1'>Perihal</label>
				<textarea
					value={payload.perihal}
					className='input-primary flex-grow '
					disabled
				/>
			</div>

			<div className='flex flex-col lg:flex-row lg:items-center'>
				<label className='w-32'>Tgl. Dibuat</label>
				<input
					value={new Date(payload.created_at).toLocaleString(
						'id-ID',
						options1
					)}
					className='input-primary flex-grow '
					disabled
				/>
			</div>

			<div className='flex flex-col lg:flex-row lg:items-center'>
				<label className='w-32'>Tgl. Valid</label>
				<input
					value={new Date(payload.waktu_valid).toLocaleString(
						'id-ID',
						options1
					)}
					className='input-primary flex-grow '
					disabled
				/>
			</div>

			<div className='flex flex-col lg:flex-row lg:items-center'>
				<label className='w-32'>Tgl. Divalidasi</label>
				<input
					value={new Date(payload.waktu_diterima).toLocaleString(
						'id-ID',
						options
					)}
					className='input-primary flex-grow '
					disabled
				/>
			</div>

			<div className='flex flex-col lg:flex-row '>
				<label className='w-32 pt-1'>Tandatangan</label>
				<div>
					<img
						className='w-96 '
						alt='user_img'
						src={baseURL + '/storage/e-sign/qr-sign/' + payload.id}
					/>
				</div>
			</div>

			{/* <div>
				<label>Perihal</label>
				<input disabled />
			</div>

			<div>
				<label>Tanggal valid</label>
				<input disabled/>
			</div>

			<div>
				<label>Waktu validasi</label>
				<input disabled/>
			</div> */}
		</div>
	);
}

export default AccStaff;
