import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import UsersESignApi from '../../../API/UsersESignApi';
import Loading from '../../../components/static/Loading';
import AccPribadi from '../component.jsx/AccPribadi';
import AccStaff from '../component.jsx/AccStaff';

function GetAcceptedEsign(props) {
	const { id } = useParams();
	const [acceptedEsign, setAcceptedEsign] = useState({});
	const [network, setNetwork] = useState({
		success: false,
		loading: true,
		error: false,
	});

	const navigate = useNavigate();

	useEffect(() => {
		UsersESignApi.getAcceptedEsign(id)
			.then((res) => {
				setAcceptedEsign(res.data.payload);
				setNetwork({
					success: false,
					loading: false,
					error: false,
				});
			})
			.catch((err) => {
				if (err) {
					navigate('/')
				}
			});
	}, [id]);

	if (network.loading) {
		return <Loading />;
	}

	return acceptedEsign.id_penerima === acceptedEsign.id_pengirim ? (
		<AccPribadi payload={acceptedEsign} />
	) : (
		<AccStaff payload={acceptedEsign} />
	);
}

export default GetAcceptedEsign;
