import {
	CheckCircleIcon,
	EyeIcon,
	EyeOffIcon,
	LockClosedIcon,
	UserIcon,
} from '@heroicons/react/outline';
import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { Link, Navigate, useLocation, useNavigate } from 'react-router-dom';
import { setPrefix } from '../../API/Api';
import UserApi from '../../API/UserApi';
import ErrorMsg from '../../components/forms/ErrorMsg';
import SmallLoading from '../../components/static/SmallLoading';
import useAuth from '../../hooks/useAuth';

function LoginPage(props) {
	const { user, setUser } = useAuth();
	const [showPass, setShowPass] = useState();
	const [success, setSucces] = useState(false);
	const [loading, setLoading] = useState(false);

	const [reqError, setReqError] = useState(false);

	const navigate = useNavigate();

	const {
		register,
		handleSubmit,
		watch,
		formState: { errors },
	} = useForm({
		criteriaMode: 'all',
	});

	async function login(data, e) {
		e.preventDefault();
		setLoading(true);
		setReqError({});

		UserApi.login(data)
			.then((res) => {
				setPrefix(res.data.payload.user.level_pengguna);
				setUser(res.data.payload.user);

				localStorage.setItem(
					'access_token',
					'Bearer ' + res.data.payload.access_token
				);
				setLoading(false);
				setSucces(true);
				navigate('/profil');
			})
			.catch((error) => {
				setReqError(error.response.data.errors);
				setSucces(false);
				setLoading(false);
			});
	}

	function togglePassword() {
		setShowPass(!showPass);
	}

	return user ? (
		<Navigate to='/profil' from='/' />
	) : (
		<div className='grid h-full'>
			<div className='w-full md:w-96 m-auto border bg-white p-5 space-y-1'>
				<div className='flex flex-row justify-center'>
					<div className=' rounded-full p-3 border-8 border-gray-400'>
						<UserIcon className='w-32 text-gray-400' />
					</div>
				</div>

				<div className='text-2xl text-center py-3 font-bold'>Masuk</div>
				<hr />

				<form onSubmit={handleSubmit(login)} className='flex flex-col gap-2'>
					<div className='flex flex-col'>
						<label>Username</label>
						<div className='flex flex-row items-center'>
							<span className='border-2 border-gray-400 border-r-0 h-10 w-10 grid flex-shrink-0 '>
								<UserIcon className='w-5 m-auto' />
							</span>
							<input
								{...register('username', {
									required: 'Username harus diisi.',
								})}
								className='input-primary h-10 w-full'
								type={'text'}
							/>
						</div>
						<ErrorMsg>
							{errors.username ? errors.username.message : null}
						</ErrorMsg>
						<ErrorMsg>
							{reqError.username ? reqError.username.map((msg) => msg) : null}
						</ErrorMsg>
					</div>

					<div className='flex flex-col'>
						<label>Password</label>
						<div className='flex flex-row items-center'>
							<span className='border-2 border-gray-400 border-r-0 h-10 w-10 grid flex-shrink-0 '>
								<LockClosedIcon className='w-5 m-auto' />
							</span>
							<div className='flex flex-row items-center w-full'>
								<input
									{...register('password', {
										required: 'Password harus diisi.',
									})}
									className='input-primary h-10 w-full'
									type={showPass ? 'text' : 'password'}
								/>
								<button
									type='button'
									className='btn-secondary w-10 h-10 flex items-center justify-center border-gray-400 rounded-none '
									onClick={togglePassword}
								>
									{showPass ? (
										<EyeIcon className='w-5' />
									) : (
										<EyeOffIcon className='w-5' />
									)}
								</button>
							</div>
						</div>
						<ErrorMsg>
							{errors.password ? errors.password.message : null}
						</ErrorMsg>
					</div>

					<hr />
					<div className='grid '>
						<button
							disabled={loading || success}
							className='btn-primary h-12 font-bold '
						>
							{loading ? (
								<SmallLoading />
							) : success ? (
								<CheckCircleIcon className='w-6 m-auto' />
							) : (
								<span>Login</span>
							)}
						</button>
					</div>
					<div className='text-center'>
						<Link to='#'>Lupa Password? Segera hubungi operator desa</Link>
					</div>
				</form>
			</div>
		</div>
	);
}

export default LoginPage;
