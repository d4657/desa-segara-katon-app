import React from 'react';

function ErrorMsg(props) {
  return (
    <span className='text-main-danger first-letter:capitalize'  >
      {props.children}
    </span>
  );
}

export default ErrorMsg;