import React, { useContext } from 'react';
import { NavLink } from 'react-router-dom';
import {
	UserIcon,
	QrcodeIcon,
	PaperClipIcon,
	InboxIcon,
	UsersIcon,
	UserCircleIcon,
} from '@heroicons/react/outline';
import SidebarContext from '../../AppState/SideBarContext';
import useAuth from '../../hooks/useAuth';
import { baseURL } from '../../API/Api';

function SmallSidebar(props) {
	const { visible, setVisible } = useContext(SidebarContext);
	const { user } = useAuth();

	function hideWhenSmall() {
		if (window.innerWidth <= 768 && visible) {
			setVisible(false);
		}
	}

	return (
		<div className='flex flex-col bg-main-sidebar'>
			<div className='grid p-2 space-y-1 bg-main-profil border-b border-main-primary-border'>
				<div className='border m-auto rounded h-12 w-12 border-main-primary-border grid overflow-hidden'>
					{user.foto ? (
						<img
							src={baseURL + '/storage/foto/' + user.foto}
							className='w-12'
							alt='foto_profil'
						></img>
					) : (
						<UserCircleIcon className='text-gray-900 w-8 m-auto' />
					)}
				</div>
			</div>
			<NavLink
				className={(navData) =>
					navData.isActive
						? 'sidebar-item-active border-l-4 border-l-main-sidebar-border-active text-white'
						: 'sidebar-item text-gray-400 border-l-main-sidebar'
				}
				to='/profil'
				onClick={() => hideWhenSmall()}
			>
				<UserIcon className='h-7 m-auto' />
			</NavLink>
			<NavLink
				className={(navData) =>
					navData.isActive
						? 'sidebar-item-active border-l-4 border-l-main-sidebar-border-active text-white'
						: 'sidebar-item text-gray-400 border-l-main-sidebar'
				}
				to='/surat'
				onClick={() => hideWhenSmall()}
			>
				<InboxIcon className='h-7 m-auto' />
			</NavLink>
			<NavLink
				className={(navData) =>
					navData.isActive
						? 'sidebar-item-active border-l-4 border-l-main-sidebar-border-active text-white'
						: 'sidebar-item text-gray-400 border-l-main-sidebar'
				}
				to='/e-sign'
				onClick={() => hideWhenSmall()}
			>
				<QrcodeIcon className='h-7 m-auto' />
			</NavLink>
			{/* <NavLink
				className={(navData) =>
					navData.isActive
						? 'sidebar-item-active border-l-4 border-l-main-sidebar-border-active text-white'
						: 'sidebar-item text-gray-400 border-l-main-sidebar'
				}
				to='/e-form'
				onClick={() => hideWhenSmall()}
			>
				<PaperClipIcon className='h-7 m-auto' />
			</NavLink> */}

			<NavLink
				className={(navData) =>
					navData.isActive
						? 'sidebar-item-active border-l-4 border-l-main-sidebar-border-active text-white'
						: 'sidebar-item text-gray-400 border-l-main-sidebar'
				}
				to='/pengguna'
				onClick={() => hideWhenSmall()}
			>
				<UsersIcon className='h-7 m-auto' />
			</NavLink>
		</div>
	);
}

export default SmallSidebar;
