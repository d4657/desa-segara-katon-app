import React, { useContext, useRef } from 'react';
import { Link, NavLink } from 'react-router-dom';
import {
	UserIcon,
	DocumentAddIcon,
	QrcodeIcon,
	PaperClipIcon,
	InboxIcon,
	UsersIcon,
	UserCircleIcon,
} from '@heroicons/react/outline';
import SidebarContext from '../../AppState/SideBarContext';
import useAuth from '../../hooks/useAuth';
import { baseURL } from '../../API/Api';

function Sidebar(props) {
	const { visible, setVisible } = useContext(SidebarContext);
	const { user } = useAuth();

	function hideWhenSmall() {
		if (window.innerWidth <= 768 && visible) {
			setVisible(false);
		}
	}

	return (
		<div className='flex flex-col'>
			<div className='grid p-4 bg-main-profil text-gray-200 gap-1 border-b border-main-primary-border'>
				<div className='border m-auto rounded-md h-44 w-44 border-main-primary-border grid overflow-hidden'>
					{user.foto ? (
						<img
							src={baseURL + '/storage/foto/' + user.foto}
							className='w-80'
							alt='foto_profil'
						></img>
					) : (
						<UserCircleIcon className='text-gray-900 w-32 m-auto' />
					)}
				</div>
				<div className='text-center text-main-primary-border font-bold'>
					{user.nama_lengkap}
				</div>
			</div>
			<NavLink
				className={(navData) =>
					navData.isActive
						? 'sidebar-item-active border-l-4 border-l-main-sidebar-border-active text-white font-bold'
						: 'sidebar-item text-gray-300 border-l-main-sidebar'
				}
				to='/profil'
				onClick={() => hideWhenSmall()}
			>
				<UserIcon className='h-5 w-8' />
				<span>Profil</span>
			</NavLink>
			<NavLink
				className={(navData) =>
					navData.isActive
						? 'sidebar-item-active border-l-4 border-l-main-sidebar-border-active text-white font-bold'
						: 'sidebar-item text-gray-300 border-l-main-sidebar'
				}
				to='/surat'
				onClick={() => hideWhenSmall()}
			>
				<InboxIcon className='h-5 w-8' />
				<span>Surat</span>
			</NavLink>
			<NavLink
				className={(navData) =>
					navData.isActive
						? 'sidebar-item-active border-l-4 border-l-main-sidebar-border-active text-white font-bold'
						: 'sidebar-item text-gray-300 border-l-main-sidebar'
				}
				to='/e-sign'
				onClick={() => hideWhenSmall()}
			>
				<QrcodeIcon className='h-5 w-8' />
				<span>E-Sign</span>
			</NavLink>

			{/* <NavLink
				className={(navData) =>
					navData.isActive
						? 'sidebar-item-active border-l-4 border-l-main-sidebar-border-active text-white font-bold'
						: 'sidebar-item text-gray-300 border-l-main-sidebar'
				}
				to='/e-form'
				onClick={() => hideWhenSmall()}
			>
				<PaperClipIcon className='h-5 w-8' />
				<span>E-Form</span>
			</NavLink> */}

			<NavLink
				className={(navData) =>
					navData.isActive
						? 'sidebar-item-active border-l-4 border-l-main-sidebar-border-active text-white font-bold'
						: 'sidebar-item text-gray-300 border-l-main-sidebar'
				}
				to='/pengguna'
				onClick={() => hideWhenSmall()}
			>
				<UsersIcon className='h-5 w-8' />
				<span>Pengguna</span>
			</NavLink>
		</div>
	);
}

export default Sidebar;
