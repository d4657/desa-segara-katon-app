import React from 'react';

function Title(props) {
	return (
		<div className='m-y-1 bg-white border-y p-5 text-2xl flex flex-row gap-x-2' >
			<div className={props.className} >{props.text}</div>
		</div>
	);
}

export default Title;

// 
// loss: 0.0348 - root_mean_squared_error: 0.1866 - mae: 0.0687 - accuracy: 0.0525 - val_loss: 0.0342 - val_root_mean_squared_error: 0.1849 - val_mae: 0.0685 - val_accuracy: 0.0699