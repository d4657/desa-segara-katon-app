import { LoginIcon, MenuIcon } from '@heroicons/react/outline';
import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import logo from '../../assets/images/logo.png';
import useAuth from '../../hooks/useAuth';

function DashHeader(props) {
	const [shown, setShown] = useState(false);
	const toggle = () => setShown(!shown);
	const { user } = useAuth();

	return (
		<div className='flex flex-row lg:px-20 justify-between h-full pb-2 px-2'>
			<div>
				<button
					onClick={() =>
						window.open('https://segarakaton.lombokutarakab.go.id/', '_blank')
					}
					className='btn-secondary inline-flex items-end space-x-3 rounded-t-none px-2'
				>
					<img className='h-8' src={logo} alt='logo_segara_katon'></img>
					<span className='text-lg'>Desa Segara Katon</span>
				</button>
			</div>
			<div className='lg:hidden'>
				{user ? (
					<Link to='/e-sign'>
						<button className='btn-primary rounded-t-none ' onClick={toggle}>
							<LoginIcon className='h-8' />
						</button>
					</Link>
				) : (
					<button className='btn-primary rounded-t-none ' onClick={toggle}>
						<MenuIcon className='h-8' />
					</button>
				)}

				{shown ? (
					<div className='flex flex-col absolute right-2 z-10 w-32 bg-white border roudned p-1'>
						<div className='justify-end h-full '>
							<Link to='/'>
								<button
									onClick={() => setShown(false)}
									className='h-full w-32 p-2 active:bg-main-primary border-b rounded-none'
								>
									Beranda
								</button>
							</Link>
							<Link to='/register'>
								<button
									onClick={() => setShown(false)}
									className='h-full w-32 p-2 active:bg-main-primary border-b rounded-none'
								>
									Register
								</button>
							</Link>
							<Link to='/login'>
								<button
									onClick={() => setShown(false)}
									className='h-full w-32 p-2 active:bg-main-primary rounded-none'
								>
									Masuk
								</button>
							</Link>
						</div>
					</div>
				) : null}
			</div>
			<div className='justify-end h-full hidden lg:flex'>
				<Link to='/'>
					<button className='h-full w-32 btn-secondary border-t-0 rounded-none'>
						Beranda
					</button>
				</Link>
				{user ? (
					<Link to='/e-sign'>
						<button className='h-full w-64 btn-primary border-t-0 rounded-none inline-flex items-center justify-center space-x-2'>
							<LoginIcon className='w-6' />
							<span>Kembali ke layanan</span>
						</button>
					</Link>
				) : (
					<>
						<Link to='/register'>
							<button className='h-full w-32 btn-secondary border-t-0 rounded-none'>
								Register
							</button>
						</Link>
						<Link to='/login'>
							<button
								to='/login'
								className='h-full w-32 btn-secondary border-t-0 rounded-none'
							>
								Masuk
							</button>
						</Link>
					</>
				)}
			</div>
		</div>
	);
}

export default DashHeader;
