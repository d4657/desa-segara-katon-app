import React from 'react';
import {} from '@heroicons/react/outline';
import logo from '../../assets/images/logo.png';

function DashContact(props) {
	return (
		<div className='flex flex-col lg:flex-row p-5 items-center gap-2'>
			<div className='w-32'>
				<img src={logo} className='h-32' alt='logo' />
			</div>
			<div className='border-t-1'>
				<div>Hubungi Kami</div>
				<div>
					<button className='hover:underline hover:text-main-primary'
						onClick={() =>
							window.open('https://segarakaton.lombokutarakab.go.id/', '_blank')
						}
					>
						Kantor Desa Segara Katon
					</button>
					<p>
						Desa Seagra Katon,Kec. Gangga, Kab. Lombok Utara, Nusa Tenggara
						Barat, Kode Pos 83353
					</p>
				</div>
			</div>
		</div>
	);
}

export default DashContact;
