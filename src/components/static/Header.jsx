import React, { useContext, useEffect, useState } from 'react';

import {
	MenuIcon,
	LogoutIcon,
	CheckCircleIcon,
} from '@heroicons/react/outline';
import SidebarContext from '../../AppState/SideBarContext';
import { AuthContext } from '../../AppState/AuthProvider';
import UserApi from '../../API/UserApi';
import SmallLoading from './SmallLoading';
import { useNavigate } from 'react-router-dom';

import logo from '../../assets/images/logo.png';

function Header(props) {
	const { visible, setVisible } = useContext(SidebarContext);
	const { user, setUser } = useContext(AuthContext);

	const [success, setSucces] = useState(false);
	const [loading, setLoading] = useState(false);

	const navigate = useNavigate();

	useEffect(() => {
		setSucces(false);
		setSucces(false);
	}, []);

	function logout() {
		setLoading(true);
		UserApi.logout().then((res) => {
			setLoading(false);
			setSucces(true);
			navigate('/beranda');
			setUser(null);
		});
	}

	return (
		<div className='flex justify-between flex-row h-full p-2'>
			<div className='w-64 flex flex-row space-x-2'>
				<button
					onClick={() => {
						setVisible(!visible);
					}}
					className='btn-primary w-12'
				>
					<MenuIcon className='w-6 m-auto' />
				</button>

				<button
					onClick={() =>
						window.open('https://segarakaton.lombokutarakab.go.id/', '_blank')
					}
					className='px-2 border-none-0 inline-flex items-end border-b p-1 border-main-primary space-x-2'
				>
					<img className='h-10' src={logo} alt='logo_segara_katon'></img>
					<span className='text-xl font-bold text-main-primary'>Segara Katon</span>
				</button>
			</div>

			<button
				onClick={logout}
				className='text-gray-700 inline-flex items-center p-2 mr-2 space-x-1'
			>
				{loading ? (
					<SmallLoading />
				) : success ? (
					<CheckCircleIcon className='w-6 m-auto' />
				) : (
					<>
						<span>Keluar</span>
						<LogoutIcon className='w-6' />
					</>
				)}
			</button>
		</div>
	);
}

export default Header;
