import React from 'react';

function WarningContainer(props) {
  return (
    <div className={'border border-main-warning bg-main-warning bg-opacity-20 '+props.className}>
      {props.children}
    </div>
  );
}

export default WarningContainer;