import React, { useEffect, useState } from 'react';
import { Outlet, useNavigate } from 'react-router-dom';
import { setPrefix } from '../../API/Api';
import UserApi from '../../API/UserApi';
import useAuth from '../../hooks/useAuth';
import AuthLoading from '../static/AuthLoading';
import DashContact from '../static/DashContact';
import DashHeader from '../static/DashHeader';
import Loading from '../static/Loading';
import LoadingPengguna from '../static/LoadingPengguna';

function UnprotectedLayout(props) {
	const { user, setUser } = useAuth();

	const [loading, setLoading] = useState(true);
	const [success, setSuccess] = useState(false);
	const navigate = useNavigate();

	useEffect(() => {
		setLoading(true);
		UserApi.profile()
			.then((res) => {
				async function sleep() {
					setUser(res.data.payload);
					setPrefix(res.data.payload.level_pengguna);
					setSuccess(true);
					await new Promise((resolve) => setTimeout(resolve, 1000));
					setLoading(false);
				}
				sleep();
			})
			.catch((err) => {
				localStorage.removeItem('access_token');
				setUser(null);
				setPrefix(null);
				setLoading(false);
			});
	}, []);

	return loading ? (
		<LoadingPengguna success={success} />
	) : (
		<div className='h-full'>
			<div className='flex flex-col h-full space-y-2'>
				<div className='h-16 bg-white border-b'>
					<DashHeader />
				</div>
				<div className='h-full mx-2'>
					<Outlet />
				</div>
				<div className='bg-white border-t'>
					<DashContact />
				</div>
			</div>
		</div>
	);
}

export default UnprotectedLayout;
