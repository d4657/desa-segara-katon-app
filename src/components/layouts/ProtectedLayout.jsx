import React, { useEffect, useRef, useState } from 'react';
import { Navigate, Outlet, useLocation, useNavigate } from 'react-router-dom';
import { setPrefix } from '../../API/Api';
import UserApi from '../../API/UserApi';
import SidebarContext from '../../AppState/SideBarContext';
import useAuth from '../../hooks/useAuth';
import AuthLoading from '../static/AuthLoading';
import Header from '../static/Header';
import Loading from '../static/Loading';
import Sidebar from '../static/Sidebar';
import SmallSidebar from '../static/SmallSidebar';

function ProtectedLayout(props) {
	const [visible, setVisible] = useState(window.innerWidth > 768);
	const { user, setUser } = useAuth();

	const [loading, setLoading] = useState(true);
	const [success, setSuccess] = useState(false);
	const navigate = useNavigate();



	useEffect(() => {
		setLoading(true);
		UserApi.profile()
			.then((res) => {
				async function sleep() {
					setUser(res.data.payload);
					setPrefix(res.data.payload.level_pengguna);
					setSuccess(true);
					await new Promise((resolve) => setTimeout(resolve, 1000));
					setLoading(false);
				}
				sleep();
			})
			.catch((err) => {
				navigate('/login');
				localStorage.removeItem('access_token');
			});
	}, []);

	return loading ? (
		<AuthLoading success={success} />
	) : (
		<SidebarContext.Provider value={{ visible, setVisible }}>
			<div className='h-full gap-y-1'>
				<div
					className={
						'border-r h-full fixed left-0 w-60 transition-all duration-200 lg:pt-0 top-16 md:top-0 md:pt-0 bg-main-sidebar z-20 ' +
						(visible ? 'ml-0 delay-100' : '-ml-60')
					}
				>
					<Sidebar visible={visible} />
				</div>

				<div
					className={
						'border-r h-full fixed top-0 left-0 w-16 transition-all duration-200 bg-main-sidebar lg:pt-0 hidden md:inline ' +
						(!visible ? 'ml-0 delay-100' : '-ml-16')
					}
				>
					<SmallSidebar />
				</div>

				<div
					className={
						'flex-grow space-y-2 transition-all duration-200 ' +
						(visible ? 'md:ml-60 delay-100' : 'lg:ml-16 md:ml-16')
					}
				>
					<div className='w-full h-16 sticky top-0 bg-white border-b z-10'>
						<Header />
					</div>

					<div
						className={
							' transition-all duration-200 ml-0 ' + (visible ? 'lg:ml-0 ' : '')
						}
					>
						<Outlet />
					</div>
				</div>
			</div>
		</SidebarContext.Provider>
	);
}

export default ProtectedLayout;
