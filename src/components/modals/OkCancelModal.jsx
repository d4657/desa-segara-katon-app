import { XIcon } from '@heroicons/react/outline';
import React, { useState } from 'react';

function OkCancelModal(props) {
	const [shown, setShown] = useState(false);

	function toggle() {
		setShown(!shown);
	}

	function ok() {
		setShown(false);
	}

	return (
		<>
			<button onClick={toggle} className={props.className}>{props.children}</button>
			{shown ? (
				<div
					onClick={() => setShown(false)}
					className='fixed top-0 left-0 w-full h-full bg-gray-800 bg-opacity-10 backdrop-blur-sm justify-center pt-2 '
				>
					<div className='w-1/2 p-2 border bg-white m-auto space-y-2 '>
						<div className='flex flex-row justify-between items-center'>
							<span className='font-bold text-lg'> {props.modalTitle} </span>
							<button className='btn-danger'>
								<XIcon className='w-4' />
							</button>
						</div>
						<hr />
						<div>
							<p>{props.modalContent}</p>
						</div>
						<hr />
						<div className='flex flex-row justify-end space-x-1 '>
							<button onClick={toggle} className='btn-secondary w-20'>
								Batal
							</button>
							<button onClick={ok} className='btn-primary w-20'>
								Oke
							</button>
						</div>
					</div>
				</div>
			) : null}
		</>
	);
}

export default OkCancelModal;
