import {
	CheckCircleIcon,
	CloudUploadIcon,
	PencilIcon,
	XCircleIcon,
	XIcon,
} from '@heroicons/react/outline';
import React, { useCallback, useEffect, useState } from 'react';
import Cropper from 'react-easy-crop';
import UserApi from '../../API/UserApi';
import useAuth from '../../hooks/useAuth';
import SmallLoading from '../static/SmallLoading';

function SignUploadModal(props) {
	const { user, setUser } = useAuth();
	const { showPhoto, setShowPhoto } = props;

	const [network, setNetwork] = useState({
		loading: false,
		success: false,
		error: false,
	});

	const [crop, setCrop] = useState({ x: 0, y: 0 });
	const [zoom, setZoom] = useState(1);
	const [preview, setPreview] = useState();
	const [selected, setSelected] = useState();
	const [cropped, setCropped] = useState({
		x: '',
		y: '',
		width: '',
		height: '',
	});

	const onCropComplete = useCallback((croppedArea, croppedAreaPixels) => {
		setCropped(croppedAreaPixels);
	}, []);

	useEffect(() => {
		if (!selected) {
			setPreview(undefined);
			return;
		}

		const objectUrl = URL.createObjectURL(selected);
		setPreview(objectUrl);

		return () => URL.revokeObjectURL(objectUrl);
	}, [selected, user.foto]);

	const onSelectFile = (e) => {
		if (!e.target.files || e.target.files.length === 0) {
			setSelected(undefined);
			return;
		}

		setSelected(e.target.files[0]);
	};

	function updateUserImages() {
		const form = new FormData();

		setNetwork({
			loading: true,
			success: false,
			error: false,
		});

		if (selected) {
			form.append('tandatangan', selected);
			form.append('x', cropped.x);
			form.append('y', cropped.y);
			form.append('width', cropped.width);
			form.append('height', cropped.height);
			UserApi.updateUserImages(form)
				.then(async (res) => {
					setUser(res.data.payload);
					setNetwork({
						loading: false,
						success: true,
						error: false,
					});
					await new Promise((resolve) => setTimeout(resolve, 2000));
					setNetwork({
						loading: false,
						success: false,
						error: false,
					});
					await new Promise((resolve) => setTimeout(resolve, 1000));
					setShowPhoto(false);
				})
				.catch(async (err) => {
					setNetwork({
						loading: false,
						success: false,
						error: true,
					});
					await new Promise((resolve) => setTimeout(resolve, 2000));
					setNetwork({
						loading: false,
						success: false,
						error: false,
					});
				});
		}
	}

	return (
		<div className='w-full h-full z-20 fixed top-0 left-0 bg-gray-800 bg-opacity-10 backdrop-blur-sm grid p-2'>
			<div className='bg-white border w-full lg:w-1/2 h-1/2  m-auto flex flex-col gap-1 p-2 rounded '>
				<div className='flex flex-row justify-between text-center'>
					<span className='font-bold'>Update tandatangan</span>
					<button
						onClick={() => setShowPhoto(!showPhoto)}
						className='btn-primary'
					>
						<XIcon className='w-5' />
					</button>
				</div>
				<div className='relative w-full h-full rounded overflow-hidden border-main-primary border '>
					{selected ? (
						<Cropper
							image={preview}
							crop={crop}
							zoom={zoom}
							aspect={2 / 1}
							onCropChange={setCrop}
							onCropComplete={onCropComplete}
							onZoomChange={setZoom}
						/>
					) : (
						<span className=' grid h-full w-full text-gray-300 relative'>
							<PencilIcon className='w-20 m-auto' />
						</span>
					)}
				</div>
				<div className='flex flex-col space-y-1'>
					<input
						onChange={onSelectFile}
						accept='image/*'
						type='file'
						className='btn-secondary flex-grow h-10 inline-flex justify-center items-center space-x-2'
					></input>
					<button
						disabled={!selected}
						onClick={() => {
							updateUserImages();
						}}
						className='btn-primary flex-grow h-10 inline-flex justify-center items-center space-x-2'
					>
						{network.loading ? (
							<SmallLoading />
						) : network.success ? (
							<CheckCircleIcon className='w-5 m-auto' />
						) : network.error ? (
							<XCircleIcon className='w-5 m-auto ' />
						) : (
							<>
								<CloudUploadIcon className='w-5' />
								<span>Simpan</span>
							</>
						)}
					</button>
				</div>
			</div>
		</div>
	);
}

export default SignUploadModal;
