import { CheckIcon, XCircleIcon, XIcon } from '@heroicons/react/outline';
import React from 'react';

function OkModal(props) {
	
	return (
		<div className='fixed top-0 left-0 w-full h-full bg-gray-800 bg-opacity-10 backdrop-blur-sm justify-center pt-2 '>
			<div className='w-full md:w-1/2 lg:w-1/3 p-2 border bg-white m-auto space-y-2 '>
				<div className='flex flex-row justify-between items-center'>
					<span 
          className='font-bold text-lg'
          >
						{props.title}
					</span>
					<button className='btn-danger'>
						<XIcon className='w-4' />
					</button>
				</div>
				<hr />
				<div>
					<p>
						{props.message}
					</p>
				</div>
        <hr/>
        <div className='flex flex-row justify-end '>
          <button onClick={props.callback} className='btn-primary w-20'>
            Ok
          </button>
        </div>
			</div>
		</div>
	);
}

export default OkModal;
