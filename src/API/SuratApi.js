import {
	Api,
	getConfig,
	getMultipartConfig,
	getPrefix,
	objectToQueryString,
} from './Api';

const SuratApi = {
	async createSurat(form) {
		return await Api.post(`${getPrefix()}/surat`, form, getConfig());
	},
	async getSurat(query) {
		return await Api.get(
			`/surat?${objectToQueryString(query)}`,
			getConfig()
		);
	},
	async getSuratById(id) {
		return await Api.get(`/surat/${id}`, getConfig());
	},
	async updateSuratById(form, id) {
		return await Api.post(
			`${getPrefix()}/surat/${id}?_method=PUT`,
			form,
			getMultipartConfig()
		);
	},
	async updateTemplateById(form, id) {
		return await Api.post(
			`${getPrefix()}/surat/template/${id}`,
			form,
			getMultipartConfig()
		);
	},
	async deleteSuratById(id) {
		return await Api.delete(`${getPrefix()}/surat/${id}`, getConfig());
	},
};

export default SuratApi;
