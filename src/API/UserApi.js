import { Api, getConfig, getPrefix, objectToQueryString } from './Api';

const UserApi = {
	async login(form) {
		return await Api.post(`/login`, form);
	},

	async logout(form) {
		return await Api.post(`/logout`, form, getConfig());
	},

	async register(form) {
		return await Api.post(`/register`, form);
	},

	async profile() {
		return await Api.get('/profile', getConfig());
	},

	async getProfile(form) {
		return await Api.get(`/${getPrefix()}/profile`, form, getConfig());
	},

	async updateBio(form) {
		return await Api.put(`/${getPrefix()}/profile/bio`, form, getConfig());
	},

	async updateAccount(form) {
		return await Api.put(`/${getPrefix()}/profile/account`, form, getConfig());
	},
	async updatePassword(form) {
		return await Api.put(`/${getPrefix()}/profile/password`, form, getConfig());
	},
	async updateUserImages(form) {
		return await Api.post(`/${getPrefix()}/profile/image`, form, getConfig());
	},
	async getAllUser(form) {
		return await Api.get(`/${getPrefix()}/user`, form, getConfig());
	},
	async getUserById(id) {
		return await Api.get(`/${getPrefix()}/user/${id}`, getConfig());
	},
	async updateBioById(form, id) {
		return await Api.put(`/${getPrefix()}/user/bio/${id}`, form, getConfig());
	},
	async updateAccountById(form, id) {
		return await Api.put(
			`/${getPrefix()}/user/account/${id}`,
			form,
			getConfig()
		);
	},
	async updatePasswordById(form, id) {
		return await Api.put(
			`/${getPrefix()}/user/password/${id}`,
			form,
			getConfig()
		);
	},
	async updateUserImagesById(form, id) {
		return await Api.post(
			`/${getPrefix()}/user/image/${id}`,
			form,
			getConfig()
		);
	},

	async getUserByType(type, query) {
		return await Api.get(
			`/users/${type}?${objectToQueryString(query)}`,
			getConfig()
		);
	},

	async deleteUserById(id) {
		return await Api.delete(`/${getPrefix()}/user/${id}`, getConfig());
	},

	async findStaffByName(query) {
		return await Api.get('/staffs?' + objectToQueryString(query), getConfig());
	},

	async upgradeAccess(form, id) {
		return await Api.put(`${getPrefix()}/user/upgrade/${id}`, form, getConfig());
	},


	
};

export default UserApi;
