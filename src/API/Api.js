import axios from 'axios';

const prefixes = ['admin', 'editor', 'staff', 'penduduk'];
let prefix = 'none';
const baseURL = 'http://localhost:8000/api';

let Api = axios.create({
	baseURL: baseURL,
});

Api.defaults.withCredentials = true;

function getConfig() {
	return {
		headers: {
			Accept: 'application/json',
			Authorization: localStorage.getItem('access_token'),
		},
	};
}

function getMultipartConfig() {
	return {
		headers: {
			Accept: 'application/json',
			'Content-Type': 'multipart/form-data;',
			Authorization: localStorage.getItem('access_token'),
		},
	};
}

function setPrefix(index) {
	if (index < 0) {
		prefix = 'admin';
	} else {
		prefix = prefixes[index];
	}
}

function getPrefix(str) {
	return prefix;
}

function objectToQueryString(obj) {
	var str = [];
	for (var p in obj)
		if (obj.hasOwnProperty(p)) {
			str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
		}
	return str.join('&');
}

export {
	Api,
	getConfig,
	getMultipartConfig,
	setPrefix,
	getPrefix,
	objectToQueryString,
	baseURL,
};
