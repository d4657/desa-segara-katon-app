import { Api, getConfig, getPrefix, objectToQueryString } from './Api';

const UsersESignApi = {
	async createUsersEsign(form) {
		return await Api.post(`/users-e-sign`, form, getConfig());
	},
	async getAllUsersEsign(query) {
		return await Api.get(
			`/users-e-sign?${objectToQueryString(query)} `,
			getConfig()
		);
	},

	async getUsersEsignId(id) {
		return await Api.get(`/users-e-sign/${id}`, getConfig());
	},

	async updateUserEsignById(form, id) {
		return await Api.put(`/users-e-sign/${id}`, form, getConfig());
	},
	async deleteEsignById(id) {
		return await Api.delete(`/users-e-sign/${id}`, getConfig());
	},
	async getAllUsersEsignByUserId(form, id) {
		return await Api.get(`/users-e-sign/user/${id}`, form, getConfig());
	},

	async acceptEsign(form, id) {
		return await Api.put('/users-e-sign/acc/' + id, form, getConfig());
	},
	
	async getAcceptedEsign(id) {
		return await Api.get(`/accepted-esign/${id}`, getConfig());
	},
};

export default UsersESignApi;
