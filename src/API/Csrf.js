import {Api, getConfig} from './Api';

const Csrf = {
  getCookie(){
    return Api.get('/csrf-cookie');
  },
}

export default Csrf