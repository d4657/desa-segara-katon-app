import {Api, getConfig} from './Api';

const ESignApi = {
	async createEsign(form) {
		return await Api.post(`/e-sign`, form, getConfig());
	},
	async getAllEsign(form) {
		return await Api.get(`/e-sign`, form, getConfig());
	},
	async getEsignById( id) {
		return await Api.get(`/e-sign/${id}`, getConfig());
	},
	async updateEsignById(form, id) {
		return await Api.put(`/e-sign/${id}`, form, getConfig());
	},
	async deleteEsignById(form, id) {
		return await Api.delete(`/e-sign/${id}`, form, getConfig());
	},
	async acceptEsignById(form, id) {
		return await Api.put(`/e-sign/acc/${id}`, form, getConfig());
	},
};

export default ESignApi;
