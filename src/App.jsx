import React, { useEffect } from 'react';
import { Navigate, Route, Routes } from 'react-router-dom';
import Csrf from './API/Csrf';
import ProtectedLayout from './components/layouts/ProtectedLayout';
import UnprotectedLayout from './components/layouts/UnprotectedLayout';
import GetAcceptedEsign from './pages/accEsignPage/:id/GetAcceptedEsign';
import TampilkanESign from './pages/esignPage/:id/TampilkanESign';
import ESignPage from './pages/esignPage/ESignPage';
import ESignPageIndex from './pages/esignPage/ESignPageIndex';
import HomePage from './pages/homePage/HomePage';
import LoginPage from './pages/loginPage/LoginPage';
import ShowUser from './pages/penggunaPage/:id/TampilkanPengguna';
import PenggunaPage from './pages/penggunaPage/PenggunaPage';
import PenggunaPageIndex from './pages/penggunaPage/PenggunaPageIndex';
import ProfilePage from './pages/profilePage/ProfilePage';
import RegisterPage from './pages/registerPage/RegisterPage';
import ShowSurat from './pages/suratPage/:id/ShowSurat';
import SuratPage from './pages/suratPage/SuratPage';
import SuratPageIndex from './pages/suratPage/SuratPageIndex';

function App() {
	useEffect(() => {
		Csrf.getCookie();
	}, []);

	return (
		<div className='App h-full'>
			<Routes>
				<Route path='/' element={<UnprotectedLayout />}>
					<Route index element={<HomePage />} />
					<Route path='beranda' element={<HomePage />} />
					<Route path='register' element={<RegisterPage />} />
					<Route path='login' element={<LoginPage />} />
					<Route path='acc'>
						<Route path=':id' element={<GetAcceptedEsign />} />
					</Route>
					<Route path='*' element={<Navigate to='/' />} />
				</Route>

				<Route path='/' element={<ProtectedLayout />}>
					<Route index element={<ProfilePage />} />
					<Route path='profil' element={<ProfilePage />} />
					<Route path='surat' element={<SuratPage />}>
						<Route index element={<SuratPageIndex />} />
						<Route path=':id' element={<ShowSurat />} />
					</Route>
					<Route path='e-sign' element={<ESignPage />}>
						<Route index element={<ESignPageIndex />} />
						<Route path=':id' element={<TampilkanESign />} />
					</Route>
					<Route path='pengguna' element={<PenggunaPage />}>
						<Route index element={<PenggunaPageIndex />} />
						<Route path=':id' element={<ShowUser />} />
					</Route>
				</Route>
			</Routes>
		</div>
	);
}

export default App;
