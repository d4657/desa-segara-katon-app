import React, { createContext, useEffect, useState } from 'react';
import { setPrefix } from '../API/Api';
import UserApi from '../API/UserApi';

const AuthContext = createContext();

function AuthProvider({ children }) {
	
	const [user, setUser] = useState(null);
	const value = { user, setUser };



	return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
}

export { AuthProvider, AuthContext };
