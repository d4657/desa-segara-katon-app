module.exports = {
	content: ['./src/**/*.{js,jsx,ts,tsx}'],
	theme: {
		extend: {
			fontFamily: {
				sans: ['Arial', 'sans-serif'],
			},

			textColor: {
				skin: {
					primary: '#232323',
					secondary: '#718198',
				},
			},

			spacing: {
        '128': '32rem',
      },

			colors: {
				main: {
					'primary' : '#4AB494',
					'primary-border' : '#1ab394',
					'primary-hover' : '#18a689',
					'danger' : '#ed5565',
					'danger-border' : '#ed5565',
					'danger-hover' : '#de4b5b',
					'warning' : '#f8ac59',
					'warning-border' : '#f8ac59',
					'warning-hover' : '#f0a656',
					'acc' : '#1c84c6',
					'acc-border' : '#1c84c6',
					'acc-hover' : '#1a7ab8',
					'profil' : '#233646',
					'sidebar' : '#2F4050',
					'sidebar-active' : '#293846',
					'sidebar-border-active' : '#46AB8D'
				},
			},
		},
	},
	plugins: [require('@tailwindcss/typography')],
};
